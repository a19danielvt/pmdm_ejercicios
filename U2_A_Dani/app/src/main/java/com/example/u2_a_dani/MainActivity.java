package com.example.u2_a_dani;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import android.os.*;
import android.view.View;
import android.widget.*;
import java.security.SecurityPermission;

public class MainActivity extends AppCompatActivity {

    private EditText textoPrincipal;
    private CheckBox checkBorrar;
    private Button boton;
    private TextView label;
    private Spinner spinner;
    private ImageView imagen;
    private RadioButton radioRojo;
    private RadioButton radioAzul;
    private Chronometer crono;
    private Switch interruptor;
    private int tiempoAutodestruccion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textoPrincipal = (EditText) findViewById(R.id.textoPrincipal);
        checkBorrar = (CheckBox) findViewById(R.id.checkBorrar);
        boton = (Button) findViewById(R.id.boton);
        label = (TextView) findViewById(R.id.label);
        spinner = (Spinner) findViewById(R.id.spinner);
        imagen = (ImageView) findViewById(R.id.imagen);
        radioAzul = (RadioButton) findViewById(R.id.radioAzul);
        radioRojo = (RadioButton) findViewById(R.id.radioRojo);
        crono = (Chronometer) findViewById(R.id.cronometro);
        interruptor = (Switch) findViewById(R.id.interruptor);

        if(radioRojo.isChecked())
            label.setTextColor(getResources().getColor(R.color.rojo));
        else
            label.setTextColor(getResources().getColor(R.color.azul));

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(parent.getSelectedItemId() == 0 | parent.getSelectedItemId() == 1){
                    Toast.makeText(getBaseContext(), R.string.text_toast_gal, Toast.LENGTH_SHORT).show();
                }else{
                    Toast.makeText(getBaseContext(), R.string.text_toast_no_gal, Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {}
        });

        crono.setOnChronometerTickListener(new Chronometer.OnChronometerTickListener() {
            @Override
            public void onChronometerTick(Chronometer chronometer) {
                // TODO Auto-generated method stub

                long tiempoPasado = SystemClock.elapsedRealtime()
                        - chronometer.getBase();
                int tiempoSeg = (int) tiempoPasado / 1000;
                if (tiempoSeg == tiempoAutodestruccion)
                    finish();
            }
        });

        interruptor.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(buttonView.isChecked()){
                    tiempoAutodestruccion = 15;
                    crono.setBase(SystemClock.elapsedRealtime());
                    crono.start();
                }else{
                    crono.stop();
                }
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        if(checkBorrar.isChecked()){
            label.setText("");
        }else
            label.setText(label.getText() + " " + textoPrincipal.getText());
    }

    public void onButtonClick(View v){
        if(checkBorrar.isChecked()){
            label.setText("");
        }else if(textoPrincipal.getText().toString().compareTo("") == 0){
            
        }
        else
            label.setText(label.getText() + " " + textoPrincipal.getText());
            textoPrincipal.setText("");
    }

    public void cambiarARojo(View v){
        label.setTextColor(getResources().getColor(R.color.rojo));
    }

    public void cambiarAAzul(View v){
        label.setTextColor(getResources().getColor(R.color.azul));
    }

    public void onImageClick(View v){
        Toast.makeText(getBaseContext(), R.string.text_image, Toast.LENGTH_SHORT).show();
    }

    public void onSwitchClick(View v){
        if(interruptor.isChecked()){
            tiempoAutodestruccion = 15;
            crono.setBase(SystemClock.elapsedRealtime());
            crono.start();
        }else{
            crono.stop();
        }
    }
}