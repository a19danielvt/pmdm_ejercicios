package com.example.a2dam_1920_2term_final10_vieites_torres_daniel;

public class Empleado {
    private int numero;
    private String nombre;
    private String telhome;
    private String telmobile;
    private int dep;

    public Empleado(){}

    public Empleado(int numero, String nombre, String telhome, String telmobile, int dep) {
        this.numero = numero;
        this.nombre = nombre;
        this.telhome = telhome;
        this.telmobile = telmobile;
        this.dep = dep;
    }

    public int getNumero(){
        return numero;
    }

    public void setNumero(int numero){
        this.numero = numero;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTelhome() {
        return telhome;
    }

    public void setTelhome(String telhome) {
        this.telhome = telhome;
    }

    public String getTelmobile() {
        return telmobile;
    }

    public void setTelmobile(String telmobile) {
        this.telmobile = telmobile;
    }

    public int getDep(){
        return dep;
    }

    public void setDep(int dep){
        this.dep = dep;
    }
}
