package com.example.a2dam_1920_2term_final10_vieites_torres_daniel;

import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.util.Log;
import android.util.Xml;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import org.xmlpull.v1.XmlPullParser;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    EditText phone;
    Spinner names;
    Button save;
    Button call;
    ListView list;

    static DatabaseManagement management;

    String dep = "https://informatica.iessanclemente.net/pmdm/dept.xml";
    String emp = "https://informatica.iessanclemente.net/pmdm/emp.xml";

    String depFile = "departamentos.txt";
    String empFile = "empleados.txt";

    String descargas = Environment.getExternalStoragePublicDirectory(
            Environment.DIRECTORY_DOWNLOADS).getAbsolutePath();

    ArrayList<Departamento> departamentos = new ArrayList<>();
    ArrayList<Empleado> empleados = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        permission();

        copiarBD();
        inicializarBD();

        phone = findViewById(R.id.phone);
        names = findViewById(R.id.names);
        save = findViewById(R.id.save);
        call = findViewById(R.id.call);
        list = findViewById(R.id.data);

        names.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mostrarTel();
                mostrarEmpleados();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String tel = phone.getText().toString();
                if(!tel.matches("[ ]{0,}")){
                    String nombre = names.getSelectedItem().toString();    
                    management.saveTel(tel, nombre);
                    Toast.makeText(MainActivity.this, "Teléfono guardado", Toast.LENGTH_SHORT).show();
                }else
                    Toast.makeText(MainActivity.this, "El campo está vacío", Toast.LENGTH_SHORT).show();
            }
        });

        call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String tel = phone.getText().toString();

                if(!tel.matches("[ ]{0,}")) {
                    Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + tel));
                    startActivity(intent);
                }else
                    Toast.makeText(MainActivity.this, "El campo está vacío", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void permission(){
        if(Build.VERSION.SDK_INT == 28){
            int external = checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE);
            int internet = checkSelfPermission(Manifest.permission.INTERNET);
            int network = checkSelfPermission(Manifest.permission.ACCESS_NETWORK_STATE);
            int call = checkSelfPermission(Manifest.permission.CALL_PHONE);

            if(external == PackageManager.PERMISSION_GRANTED &&
                internet == PackageManager.PERMISSION_GRANTED &&
                network == PackageManager.PERMISSION_GRANTED &&
                call == PackageManager.PERMISSION_GRANTED){

                Descargas des = new Descargas();
                des.execute();

            }else{
                requestPermissions(new String[]{
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.INTERNET,
                        Manifest.permission.ACCESS_NETWORK_STATE,
                        Manifest.permission.CALL_PHONE}, 1);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if(requestCode == 1){
            boolean permitidos = true;
            for(int i : grantResults){
                if(i == PackageManager.PERMISSION_DENIED){
                    permitidos = false;
                }
            }

            if(permitidos){
                Descargas des = new Descargas();
                des.execute();
            }
            else
                Toast.makeText(this, "FALTA UN PERMISO", Toast.LENGTH_SHORT).show();
        }
    }

    public void copiarBD(){
        String bddestino = "/data/data/" + getPackageName() + "/databases/"
                + DatabaseManagement.DATABASE_NAME;
        File file = new File(bddestino);
        if (file.exists()) {
            Log.i("BD", "YA EXISTE LA BASE DE DATOS");
            return;
        }

        String pathbd = "/data/data/" + getPackageName()
                + "/databases/";
        File filepathdb = new File(pathbd);
        filepathdb.mkdirs();

        try {
            InputStream inputstream = getAssets().open(DatabaseManagement.DATABASE_NAME);
            OutputStream outputstream = new FileOutputStream(bddestino);

            int tamread;
            byte[] buffer = new byte[2048];

            while ((tamread = inputstream.read(buffer)) > 0) {
                outputstream.write(buffer, 0, tamread);
            }

            inputstream.close();
            outputstream.flush();
            outputstream.close();
            Toast.makeText(getApplicationContext(), "BASE DE DATOS COPIADA", Toast.LENGTH_LONG).show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void inicializarBD(){
        management = new DatabaseManagement(this);
        management.db = management.getWritableDatabase();
    }

    public void descargarXml(){
        if(!isConnected()){
            Toast.makeText(this, "No hay conexión a internet", Toast.LENGTH_SHORT).show();
            return;
        }

        String[] urls = new String[]{dep, emp};

        for (int i = 0; i < urls.length; i++) {
            URL url = null;
            try {
                url = new URL(urls[i]);
            } catch (Exception e) {
                Log.e("ERROR", "Error con la URL");
                return;
            }
            String archivo = (i == 0)? descargas + "/" + depFile : descargas + "/" + empFile;

            try{
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(10000);
                conn.setConnectTimeout(15000);
                conn.setRequestMethod("POST");
                conn.setDoInput(true);

                conn.connect();

                int response = conn.getResponseCode();
                if (response != HttpURLConnection.HTTP_OK){
                    Toast.makeText(this, "No se puede acceder a la url", Toast.LENGTH_LONG).show();
                    return;
                }

                OutputStream os = new FileOutputStream(archivo);
                InputStream in = conn.getInputStream();

                byte[] data = new byte[1024];
                int count;
                while((count = in.read(data)) != -1){
                    os.write(data, 0, count);
                }

                os.flush();
                os.close();
                in.close();
                conn.disconnect();
                Log.e("DESCARGADO", ":)");
            }catch(Exception e){
                e.printStackTrace();
            }
        }
    }

    public boolean isConnected(){
        ConnectivityManager conn = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo net = conn.getActiveNetworkInfo();

        if(net != null && net.isConnected()){
            return true;
        }

        return false;
    }

    public void procesarXml(File[] archivos){
        try{
            Departamento dep = null;
            Empleado emp = null;

            for (int i = 0; i < archivos.length; i++) {
                InputStream is = new FileInputStream(archivos[i]);

                XmlPullParser parser = Xml.newPullParser();
                parser.setInput(is, "UTF-8");

                int tag = parser.nextTag();
                while(tag != XmlPullParser.END_DOCUMENT){
                    if(tag == XmlPullParser.START_TAG){
                        if(parser.getName().equals("dept")){
                            dep = new Departamento();

                            parser.nextTag();
                            dep.setNumero(Integer.parseInt(parser.nextText()));

                            tag = parser.nextTag();
                            dep.setNombre(parser.nextText());
                        }
                        if(parser.getName().equals("emp")){
                            emp = new Empleado();

                            parser.nextTag();
                            emp.setNumero(Integer.parseInt(parser.nextText()));

                            parser.nextTag();
                            emp.setNombre(parser.nextText());

                            parser.nextTag();
                            while(parser.getName().equals("tel")){
                                if(parser.getAttributeValue(null, "type").equals("home")) {
                                    emp.setTelhome(parser.nextText());
                                }else{
                                    emp.setTelmobile(parser.nextText());
                                }
                                parser.nextTag();
                            }

                            emp.setDep(Integer.parseInt(parser.nextText()));
                        }
                    }
                    if(tag == XmlPullParser.END_TAG){
                        if(parser.getName().equals("dept")){
                            departamentos.add(dep);
                        }
                        if(parser.getName().equals("emp")){
                            empleados.add(emp);
                        }
                    }

                    tag = parser.next();
                }

                is.close();
            }
        }catch(Exception e){
            e.printStackTrace();
        }

        Log.e("XML", "PROCESADOS");
    }

    public void departamentosSpinner(){
        ArrayList<String> nombres = new ArrayList<>();

        for(Departamento dep : departamentos){
            nombres.add(dep.getNombre());
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, nombres);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        names.setAdapter(adapter);
    }

    public void mostrarTel(){
        String dep = names.getSelectedItem().toString();
        String tel = management.getTel(dep) + "";

        if(tel.equals("null"))
            phone.setText("");
        else
            phone.setText(tel);
    }

    public void mostrarEmpleados(){
        String nombre = names.getSelectedItem().toString();

        ArrayList<Empleado> emps = management.getAllEmp(nombre);
        ArrayList<String> nombres = new ArrayList<>();

        for(Empleado emp : emps){
            nombres.add(emp.getNombre());
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, nombres);
        list.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.preferencias){
            Intent intent = new Intent(getApplicationContext(), PantallaPreferencias.class);
            startActivity(intent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        SharedPreferences preferencias = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        String color = preferencias.getString("color", getResources().getString(R.string.default_value));
        phone.setTextColor(Color.parseColor(color));
    }

    private class Descargas extends AsyncTask<Void, Void, Boolean>{

        @Override
        protected Boolean doInBackground(Void... voids) {
            descargarXml();

            File dep = new File(descargas + "/" + depFile);
            File emp = new File(descargas + "/" + empFile);

            File[] archivos = new File[]{dep, emp};

            procesarXml(archivos);

            management.insertarDatos(departamentos, empleados);
            return true;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            if(aBoolean) {
                departamentosSpinner();
                mostrarTel();
                mostrarEmpleados();
            }
        }
    }
}