package com.example.a2dam_1920_2term_final10_vieites_torres_daniel;

import android.app.Activity;
import android.os.Bundle;
import android.preference.PreferenceFragment;

public class PantallaPreferencias extends Activity {

    public static class SettingsFragment extends PreferenceFragment{
        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.preferencias);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getFragmentManager().beginTransaction().replace(android.R.id.content,
                new SettingsFragment()).commit();
    }
}
