package com.example.a2dam_1920_2term_final10_vieites_torres_daniel;

import java.util.ArrayList;

public class Departamento {
    private int numero;
    private String nombre;
    private String tel;
    private ArrayList<Empleado> empleados;

    public Departamento(){}

    public Departamento(int numero, String nombre, String tel) {
        this.numero = numero;
        this.nombre = nombre;
        this.tel = tel;
    }

    public int getNumero(){
        return numero;
    }

    public void setNumero(int numero){
        this.numero = numero;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public ArrayList<Empleado> getEmpleados() {
        return empleados;
    }

    public void setEmpleados(ArrayList<Empleado> empleados) {
        this.empleados = empleados;
    }
}
