package com.example.a2dam_1920_2term_final10_vieites_torres_daniel;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;

public class DatabaseManagement extends SQLiteOpenHelper {

    SQLiteDatabase db;

    final static String DATABASE_NAME = "base.db";

    // TABLAS
    final static String DEP = "departamentos";
    final static String EMP = "empleados";

    // COLUMNAS
    final static String DEPTNO = "deptno";
    final static String DENAME = "dename";
    final static String TEL = "tel";
    final static String EMPNO = "empno";
    final static String ENAME = "ename";
    final static String TELHOME = "telhome";
    final static String TELMOBILE = "telmobile";


    public DatabaseManagement(Context context) {
        super(context, DATABASE_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public void insertarDatos(ArrayList<Departamento> deps, ArrayList<Empleado> emps){
        for(Departamento dep : deps) {
            if (getDep(dep.getNumero()) == null) {
                db.execSQL("insert into " + DEP + " values " + "(?, ?, ?)",
                        new Object[]{dep.getNumero(), dep.getNombre(), dep.getTel()});
            }
        }

        for(Empleado emp : emps){
            if(getEmp(emp.getNumero()) == null){
                db.execSQL("insert into " + EMP + " values " + "(?, ?, ?, ?, ?)",
                        new Object[]{emp.getNumero(), emp.getNombre(), emp.getTelhome(),
                                emp.getTelmobile(), emp.getDep()});
            }
        }
    }

    public Departamento getDep(int numero){
        Cursor cursor = db.rawQuery("select * from " + DEP + " where " + DEPTNO + "=?",
                new String[]{Integer.toString(numero)});

        Departamento dep = null;
        if(cursor.moveToFirst()){
            dep = new Departamento();
            while(!cursor.isAfterLast()){
                dep.setNumero(cursor.getInt(0));
                dep.setNombre(cursor.getString(1));
                dep.setTel(cursor.getString(2));

                cursor.moveToNext();
            }
        }

        return dep;
    }

    public Empleado getEmp(int numero){
        Cursor cursor = db.rawQuery("select * from " + EMP + " where " + EMPNO + "=?",
                new String[]{Integer.toString(numero)});

        Empleado emp = null;
        if(cursor.moveToFirst()){
            emp = new Empleado();
            while(!cursor.isAfterLast()){
                emp.setNumero(cursor.getInt(0));
                emp.setNombre(cursor.getString(1));
                emp.setTelhome(cursor.getString(2));
                emp.setTelmobile(cursor.getString(3));
                emp.setDep(cursor.getInt(4));

                cursor.moveToNext();
            }
        }

        return emp;
    }

    public String getTel(String nombre){
        Cursor cursor = db.rawQuery("select " + TEL + " from " + DEP + " where " + DENAME + "=?",
                new String[]{nombre});

        String tel = null;

        if(cursor.moveToFirst()){
            tel = cursor.getString(0);
        }

        return tel;
    }

    public void saveTel(String tel, String nombre){
        db.execSQL("update " + DEP + " set " + TEL + "=? where " + DENAME + "=?",
                new String[]{tel, nombre});
    }

    public ArrayList<Empleado> getAllEmp(String nombre){
        ArrayList<Empleado> emps = new ArrayList<>();

        String dep = getDepNumber(nombre);
        Cursor cursor = db.rawQuery("select * from " + EMP + " where " + DEPTNO + "=?",
                new String[]{dep});

        if(cursor.moveToFirst()){
            while(!cursor.isAfterLast()){
                Empleado emp = new Empleado();

                emp.setNumero(cursor.getInt(0));
                emp.setNombre(cursor.getString(1));
                emp.setTelhome(cursor.getString(2));
                emp.setTelmobile(cursor.getString(3));
                emp.setDep(cursor.getInt(4));

                emps.add(emp);

                cursor.moveToNext();
            }
        }

        return emps;
    }

    public String getDepNumber(String nombre){
        Cursor cursor = db.rawQuery("select " + DEPTNO + " from " + DEP + " where " + DENAME + "=?",
                new String[]{nombre});

        cursor.moveToFirst();

        return Integer.toString(cursor.getInt(0));
    }
}
