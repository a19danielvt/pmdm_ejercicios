package com.example.ud_a_45_c_a19danielvt;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.util.Xml;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import org.xmlpull.v1.XmlPullParser;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;

public class Principal extends AppCompatActivity {

    private Downloader downloader;
    private Database database;

    private EditText url;

    public static enum NET{WITHOUT, WITH}

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        url = findViewById(R.id.url);

        permission();

        copiarBD();
    }

    @Override
    protected void onStart() {
        super.onStart();

        if(database == null){
            database = new Database(this);
            database.db = database.getWritableDatabase();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();

        if(database != null){
            database.close();
            database = null;
        }
    }

    public void permission(){
        if (Build.VERSION.SDK_INT == 28){
            int internet = checkSelfPermission(Manifest.permission.INTERNET);
            int net = checkSelfPermission(Manifest.permission.ACCESS_NETWORK_STATE);
            if (internet == PackageManager.PERMISSION_DENIED ||
                    net == PackageManager.PERMISSION_DENIED){

                requestPermissions( new String[]{
                        Manifest.permission.ACCESS_NETWORK_STATE,
                        Manifest.permission.INTERNET
                }, 1);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if(requestCode == 1){
            for(int grantResult : grantResults){
                if(grantResult == PackageManager.PERMISSION_DENIED){
                    Toast.makeText(this, "FALTA UN PERMISO", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    public void copiarBD(){
        try {
            String ruta_string = "/data/data/" + getPackageName() + "/databases/" + Database.DATABASE_NAME;
            File file = new File(ruta_string);

            if (file.exists())
                return;

            String pathbd = "/data/data/" + getPackageName() + "/databases/";
            File filepathdb = new File(pathbd);
            filepathdb.mkdirs();

            InputStream inputstream = getAssets().open(Database.DATABASE_NAME);
            OutputStream outputstream = new FileOutputStream(ruta_string);

            int tamread;
            byte[] buffer = new byte[2048];

            while ((tamread = inputstream.read(buffer)) > 0) {
                outputstream.write(buffer, 0, tamread);
            }

            inputstream.close();
            outputstream.flush();
            outputstream.close();
            Toast.makeText(getApplicationContext(), "BASE DE DATOS COPIADA", Toast.LENGTH_LONG).show();
        } catch (Exception e) {
            Log.e("ERROR", "Error al copiar la base de datos: " + e.getMessage());
        }
    }

    public void download(View v){
        if(getNet() == NET.WITHOUT){
            Toast.makeText(this, "NO HAY CONEXIÓN A INTERNET", Toast.LENGTH_SHORT).show();
            return;
        }

        if(url.getText().toString().matches("[ ]{0,}")){
            Toast.makeText(this, "INTRODUCE UNA URL", Toast.LENGTH_SHORT).show();
            return;
        }

        if(downloader == null || downloader.getStatus() == AsyncTask.Status.FINISHED){
            downloader = new Downloader(this, url.getText().toString());
            downloader.execute();
        }
        else{
            Toast.makeText(this, "ESTÁ DESCARGANDO", Toast.LENGTH_SHORT).show();
        }
    }

    public NET getNet(){
        ConnectivityManager conn = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo net = conn.getActiveNetworkInfo();
        if(net != null && net.isConnected()){
            return NET.WITH;
        }

        return NET.WITHOUT;
    }

    public void processXml(View v){
        File file = new File(getFilesDir(),
                Uri.parse(url.getText().toString()).getLastPathSegment());
        if(!file.exists()){
            Toast.makeText(this, "NO EXISTE EL ARCHIVO XML", Toast.LENGTH_SHORT).show();
            return;
        }

        ArrayList<Salary> salaries = new ArrayList<>();

        try {
            InputStream is = new FileInputStream(file);
            XmlPullParser xml = Xml.newPullParser();
            xml.setInput(is, "UTF-8");

            int tag = xml.nextTag();

            Salary salary = null;
            while(tag != XmlPullParser.END_DOCUMENT){
                if(tag == XmlPullParser.START_TAG){
                    if(xml.getName().equals("salary")){
                        salary = new Salary();

                        xml.nextTag(); //month
                        salary.setMonth(xml.nextText());

                        xml.nextTag(); //amount
                        salary.setTotal_salary(Integer.parseInt(xml.nextText()));

                        tag = xml.nextTag();
                        while(xml.getName().equals("complement")){
                            salary.addToSalary(Integer.parseInt(xml.nextText()));
                            tag = xml.nextTag();
                        }
                    }
                }
                if(tag == XmlPullParser.END_TAG){
                    if(xml.getName().equals("salary")){
                        salaries.add(salary);
                    }
                }

                tag = xml.nextTag();
            }
        }catch(Exception e){
            System.out.println(e.getMessage());
        }

        addToDB(salaries);

        Toast.makeText(this, "PROCESADO", Toast.LENGTH_SHORT).show();
    }

    public void addToDB(ArrayList<Salary> salaries){
        for(Salary salary : salaries){
            if(!database.hasData(salary.getMonth())){
                database.insert(salary);
            }
        }
    }

    public void showSalaries(View v){
        ListView data = findViewById(R.id.data);
        ArrayList<Salary> salaries = database.getSalariesList();

        if(salaries.size() == 0){
            Toast.makeText(this, "NO HAY DATOS", Toast.LENGTH_SHORT).show();
            return;
        }

        ArrayList<String> salaries_data = new ArrayList<>();
        for(Salary salary : salaries){
            salaries_data.add(salary.getTotal_salary() + "\t\t" + salary.getMonth());
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_list_item_1, salaries_data);
        data.setAdapter(adapter);
    }
}