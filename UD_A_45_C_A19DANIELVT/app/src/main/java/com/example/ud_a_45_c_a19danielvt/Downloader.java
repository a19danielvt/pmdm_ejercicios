package com.example.ud_a_45_c_a19danielvt;

import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class Downloader extends AsyncTask <Void, Integer, Boolean>{

    private Context context;
    private String url_string;
    private File ruta;

    private String mensaje;

    public Downloader(Context context, String url_string){
        this.context = context;
        this.url_string = url_string;
    }

    @Override
    protected void onPreExecute() {
        Toast.makeText(context, "DESCARGANDO", Toast.LENGTH_SHORT).show();
        ruta = new File(context.getFilesDir(), Uri.parse(url_string).getLastPathSegment());
    }

    @Override
    protected Boolean doInBackground(Void... voids) {
        try{
            URL url = new URL(url_string);

            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000);
            conn.setDoInput(true);
            conn.setRequestMethod("POST");
            conn.setConnectTimeout(15000);

            conn.connect();

            int response = conn.getResponseCode();
            if(response != HttpURLConnection.HTTP_OK){
                mensaje = "NO SE PUEDE DESCARGAR EL ARCHIVO";
                cancel(true);
            }

            OutputStream os = new FileOutputStream(ruta);
            InputStream in = conn.getInputStream();
            byte data[] = new byte[1024];
            int count;
            while ((count = in.read(data)) != -1) {
                os.write(data, 0, count);
            }
            os.flush();
            os.close();
            in.close();
            conn.disconnect();
        
        }catch(Exception e){
            Log.e("ERROR", "Error descargando el archivo");
        }

        return true;
    }

    @Override
    protected void onCancelled() {
        Toast.makeText(context, mensaje, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onPostExecute(Boolean aBoolean) {
        Toast.makeText(context, "ARCHIVO DESCARGADO", Toast.LENGTH_SHORT).show();
    }
}
