package com.example.ud_a_45_c_a19danielvt;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

public class Database extends SQLiteOpenHelper {
    SQLiteDatabase db;

    final static String DATABASE_NAME = "salaries.db";
    final static String DATABASE_TABLE = "salary";
    final static String MONTH_FIELD = "month";
    final static String TOTAL_FIELD = "total_salary";

    public Database(Context context){
        super(context, DATABASE_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public void insert(Salary salary){
        db.execSQL("insert into " + DATABASE_TABLE + " values (\"" + salary.getMonth() +
                "\", " + salary.getTotal_salary() + ")");
    }

    public boolean hasData(String month){
        Cursor cursor = db.rawQuery("select " + TOTAL_FIELD + " from " + DATABASE_TABLE + " where month=?",
                new String[]{month});

        return cursor.moveToFirst();
    }

    public ArrayList<Salary> getSalariesList(){
        ArrayList<Salary> salaries = new ArrayList<>();

        Cursor cursor = db.rawQuery("select * from " + DATABASE_TABLE, null);
        if(cursor.getCount() > 0){
            while(cursor.moveToNext()){
                String month = cursor.getString(cursor.getColumnIndex(MONTH_FIELD));
                int total_salary = cursor.getInt(cursor.getColumnIndex(TOTAL_FIELD));
                Salary salary = new Salary(month, total_salary);
                salaries.add(salary);
            }
        }

        return salaries;
    }
}
