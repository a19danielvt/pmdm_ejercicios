package com.example.ud_a_45_c_a19danielvt;

public class Salary {
    private String month;
    private int total_salary;

    public Salary(){}

    public Salary(String month, int total_salary){
        this.month = month;
        this.total_salary = total_salary;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month){
        this.month = month;
    }

    public int getTotal_salary() {
        return total_salary;
    }

    public void setTotal_salary(int total_salary){
        this.total_salary = total_salary;
    }

    public void addToSalary(int toAdd) {
        this.total_salary += toAdd;
    }
}
