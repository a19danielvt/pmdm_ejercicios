package com.example.ud_a1a_a19danielvt;

import android.content.*;
import android.database.Cursor;
import android.database.sqlite.*;

import java.util.ArrayList;

public class ControlBD extends SQLiteOpenHelper {

    SQLiteDatabase consulta;

    final static String nombreBD = "DATOS";
    static boolean manual = false;

    final static String crearTablas =
            "CREATE TABLE PERSOAS(" +
            "nome VARCHAR(50) PRIMARY KEY," +
            "descricion VARCHAR(500))";

    public ControlBD(Context context) {
        super(context, nombreBD, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        if(manual){
            db.execSQL(crearTablas);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public ArrayList<Persoas> obtenerPersonas(){
        ArrayList<Persoas> personas = new ArrayList<>();

        String todosLosDatos = "SELECT * FROM PERSOAS";
        Cursor datos = consulta.rawQuery(todosLosDatos, null);

        if(datos.moveToFirst()) {
            Persoas p;
            while (!datos.isAfterLast()) {
                p = new Persoas(datos.getString(0), datos.getString(1));
                personas.add(p);
                datos.moveToNext();
            }
        }

        return personas;
    }

    public void añadirPersona(Persoas p){
        ContentValues datos = new ContentValues();
        datos.put("nome", p.getNome());
        datos.put("descricion", p.getDescricion());
        consulta.insert("PERSOAS", null, datos);
    }
}
