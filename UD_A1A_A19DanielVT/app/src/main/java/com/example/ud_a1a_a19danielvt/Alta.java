package com.example.ud_a1a_a19danielvt;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Alta extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alta);

        EditText nombre = findViewById(R.id.nombre);
        EditText descripcion = findViewById(R.id.descripcion);

        Button alta = findViewById(R.id.darAlta);
        alta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(nombre.getText().toString().equals("") || descripcion.getText().toString().equals("")){
                    Toast.makeText(getApplicationContext(), "Todos los campos tienen que estar cubiertos",
                            Toast.LENGTH_SHORT).show();
                }
                else{
                    Persoas p = new Persoas(nombre.getText().toString(), descripcion.getText().toString());
                    PantallaPrincipal.control.añadirPersona(p);
                    Toast.makeText(getApplicationContext(), "Persona añadida a la base de datos",
                            Toast.LENGTH_SHORT).show();

                    nombre.setText("");
                    descripcion.setText("");
                }
            }
        });
    }
}