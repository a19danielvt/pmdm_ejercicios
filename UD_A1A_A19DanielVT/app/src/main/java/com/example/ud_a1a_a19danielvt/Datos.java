package com.example.ud_a1a_a19danielvt;

import androidx.appcompat.app.AppCompatActivity;

import android.content.SharedPreferences;
import android.os.*;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.*;

import java.io.*;
import java.util.ArrayList;

public class Datos extends AppCompatActivity {

    ListView lista;
    int seleccionado = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_datos);

        lista = findViewById(R.id.lista);

        mostrarDatos();

        Button guardar = findViewById(R.id.guardar);
        guardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(seleccionado == -1){
                    Toast.makeText(getApplicationContext(), "No hay ningún item seleccionado",
                            Toast.LENGTH_SHORT).show();
                }
                else{
                    SharedPreferences preferencias = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

                    String carpeta = preferencias.getString("carpeta", "DATOS");
                    File carpetaSDCard = Environment.getExternalStoragePublicDirectory(carpeta);

                    if(!carpetaSDCard.exists())
                        carpetaSDCard.mkdirs();

                    Persoas p = (Persoas) lista.getItemAtPosition(seleccionado);

                    File ruta = new File(carpetaSDCard, p.getNome() + ".txt");

                    try{
                        OutputStreamWriter os = new OutputStreamWriter(new FileOutputStream(ruta));

                        String texto = p.getNome() + ":\n\n" + p.getDescricion();

                        os.write(texto);

                        os.flush();
                        os.close();
                    }catch(Exception e){
                        Log.e("ERROR", "Fallo al escribir en el archivo: " + e.getMessage());
                    }

                    Toast.makeText(getApplicationContext(), "Archivo creado", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void mostrarDatos(){

        ArrayList<Persoas> personas = PantallaPrincipal.control.obtenerPersonas();
        ArrayAdapter<Persoas> adapter = new ArrayAdapter<Persoas>(getApplicationContext(),
                android.R.layout.simple_list_item_1, personas);
        lista.setAdapter(adapter);

        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                TextView tv = findViewById(R.id.tv);
                tv.setText(((Persoas)parent.getItemAtPosition(position)).getDescricion());

                seleccionado = position;
            }
        });
    }
}