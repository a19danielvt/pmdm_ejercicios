package com.example.ud_a1a_a19danielvt;

public class Persoas {

    private String nome;
    private String descricion;

    public Persoas(String nome, String descricion) {
        this.nome = nome;
        this.descricion = descricion;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDescricion() {
        return descricion;
    }

    public void setDescricion(String descricion) {
        this.descricion = descricion;
    }

    public String toString(){
        return nome;
    }
}
