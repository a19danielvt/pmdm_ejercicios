package com.example.ud_a1a_a19danielvt;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class PantallaPrincipal extends AppCompatActivity {

    static ControlBD control;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        permisos();

        String ruta = "/data/data/" + getPackageName() + "/databases/" + ControlBD.nombreBD;
        File file = new File(ruta);

        if(!file.exists()){
            mostrarDialogo();
        }else {
            inicializar();
        }
    }

    public void permisos(){
        if (Build.VERSION.SDK_INT >= 23){
            int permiso = checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE);
            if (permiso == PackageManager.PERMISSION_GRANTED){}
            else{
                PantallaPrincipal.this.requestPermissions(new String[]{
                        Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults){
        switch (requestCode) {
            case 1: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                } else {
                    Toast.makeText(this,"ES NECESARIO EL PERMISO PARA ESCRIBIR EN LA SDCARD EXTERNA",
                            Toast.LENGTH_LONG).show();
                }
                return;
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menupreferencias, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if(item.getItemId() == R.id.preferencias){
            Intent intent = new Intent(getApplicationContext(), Preferencias.class);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void mostrarDialogo(){
        AlertDialog.Builder dialogo = new AlertDialog.Builder(this);
        dialogo.setCancelable(false);
        dialogo.setTitle("Opciones de creación")
                .setItems(R.array.opcionesCreacion, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if(which == 0){
                            ControlBD.manual = true;
                            inicializar();
                            Toast.makeText(getApplicationContext(), "Base de datos creada manualmente",
                                    Toast.LENGTH_SHORT).show();
                        }
                        else{
                            copiarDB();
                        }
                    }
                });

        dialogo.create().show();
    }

    public void copiarDB(){
        String rutaCarpeta = "/data/data/" + getPackageName() + "/databases/";
        File carpeta = new File(rutaCarpeta);
        carpeta.mkdirs();

        try{
            String ruta = rutaCarpeta + ControlBD.nombreBD;

            InputStream is = getAssets().open(ControlBD.nombreBD);
            OutputStream os = new FileOutputStream(ruta);

            int tam;
            byte[] buffer = new byte[2048];

            while((tam = is.read(buffer)) > 0)
                os.write(buffer, 0, tam);

            is.close();
            os.flush();
            os.close();

            Toast.makeText(this, "BASE DE DATOS COPIADA", Toast.LENGTH_SHORT).show();
        }catch(Exception e){
            Log.e("ERROR", "Fallo al copiar la base de datos");
        }

        inicializar();
    }

    public void inicializar(){
        control = new ControlBD(this);
        control.consulta = control.getWritableDatabase();
    }

    public void darDeAlta(View v){
        Intent intent = new Intent(this, Alta.class);
        startActivity(intent);
    }

    public void mostrarDatos(View v){
        Intent intent = new Intent(this, Datos.class);
        startActivity(intent);
    }
}