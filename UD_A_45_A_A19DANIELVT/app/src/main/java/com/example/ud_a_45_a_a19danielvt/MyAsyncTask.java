package com.example.ud_a_45_a_a19danielvt;

import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class MyAsyncTask extends AsyncTask<Void, Integer, Boolean> {

    MainActivity main;
    private String archivo = "https://manuais.iessanclemente.net/images/2/20/Platega_pdm_rutas.xml";
    private File carpeta;
    private File ruta;
    URL url;

    private String mensaje = "";
    
    public MyAsyncTask(MainActivity main, File carpeta){
        this.main = main;
        this.carpeta = carpeta;
    }

    @Override
    protected Boolean doInBackground(Void... voids) {
        try {
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(15000);
            conn.setRequestMethod("POST");
            conn.setDoInput(true);

            conn.connect();

            int response = conn.getResponseCode();
            if(response == HttpURLConnection.HTTP_OK){
                Toast.makeText(main, "No se puede descargar el archivo", Toast.LENGTH_SHORT).show();
                return false;
            }

            OutputStream os = new FileOutputStream(ruta);
            InputStream in = conn.getInputStream();
            byte data[] = new byte[1024];	// Buffer a utilizar
            int count;
            while ((count = in.read(data)) != -1) {
                os.write(data, 0, count);
            }
            os.flush();
            os.close();
            in.close();
            conn.disconnect();
        } catch (Exception e) {
            cancel(true);
            Log.e("ERROR", "Error abriendo la conexión " + e.getMessage());
            return false;
        }

        return true;
    }

    @Override
    protected void onPreExecute() {
        ruta = new File(carpeta, Uri.parse(archivo).getLastPathSegment());
        if(ruta.exists()){
            mensaje = "EL ARCHIVO YA EXISTE";
            cancel(true);
        }
        try {
            url = new URL(archivo);
        } catch (Exception e) {
            Log.e("ERROR", "Error al crear la URL" + e.getMessage());
        }
    }

    @Override
    protected void onPostExecute(Boolean aBoolean) {
        Toast.makeText(main, "ARCHIVO DESCARGADO", Toast.LENGTH_SHORT).show();
        main.showData(ruta);
    }

    @Override
    protected void onCancelled() {
        Toast.makeText(main, mensaje, Toast.LENGTH_SHORT).show();
        main.showData(ruta);
    }
}
