package com.example.ud_a_45_a_a19danielvt;

import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.util.Xml;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import org.xmlpull.v1.XmlPullParser;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

public class MainActivity extends AppCompatActivity {

    private final File externa = Environment.getExternalStorageDirectory();
    private final String rutes = "/RUTAS/";

    private MyAsyncTask hilo;

    public static enum RED {CONRED, SINRED};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        permission();

        if(getRed() == RED.SINRED){
            Toast.makeText(this, "No se puede utilizar la aplicación sin red", Toast.LENGTH_SHORT).show();
            finish();
        }
    }

    public RED getRed(){
        NetworkInfo net = null;

        ConnectivityManager conn = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        net = conn.getActiveNetworkInfo();

        if(net != null && net.isConnected()){
            return RED.CONRED;
        }

        return RED.SINRED;
    }

    public void download(View v){
        if(hilo == null || hilo.getStatus() == AsyncTask.Status.FINISHED){
            hilo = new MyAsyncTask(this, new File(externa, rutes));
            hilo.execute();
        }
        else
            Toast.makeText(this, "ESTÁ DESCARGANDO", Toast.LENGTH_SHORT).show();
    }

    public void showData(File ruta){
        TextView data = (TextView) findViewById(R.id.datos);
        data.setText("");
        try {
            InputStream is = new FileInputStream(ruta);
            XmlPullParser xml = Xml.newPullParser();
            xml.setInput(is, "UTF-8");

            int linea = xml.next();
            while(linea != XmlPullParser.END_DOCUMENT){
                if(linea == XmlPullParser.START_TAG){
                    if(xml.getName().equals("ruta")){
                        data.append("Ruta:\n");
                        linea = xml.nextTag();
                        data.append("\tNome: " + xml.nextText() + "\n");
                        linea = xml.nextTag();
                        data.append("\tDescrición: " + xml.nextText() + "\n");
                    }
                }

                linea = xml.next();
            }

            is.close();
        } catch (Exception e) {
            Log.e("ERROR", "Error mostrando los datos " + e.getMessage());
        }
    }

    public void permission(){
        if(Build.VERSION.SDK_INT == 28) {
            int escribir = checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE);
            int internet = checkSelfPermission(Manifest.permission.INTERNET);
            int access = checkSelfPermission(Manifest.permission.ACCESS_NETWORK_STATE);
            if(escribir == PackageManager.PERMISSION_GRANTED &&
                internet == PackageManager.PERMISSION_GRANTED &&
                access == PackageManager.PERMISSION_GRANTED){

                File folder = new File(externa, rutes);
                if(!folder.exists()){
                    folder.mkdirs();
                }
            }
            else{
                requestPermissions(new String[] {
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.INTERNET,
                        Manifest.permission.ACCESS_NETWORK_STATE
                }, 1);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        if(requestCode == 1){
            for(int grantResult : grantResults) {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResult == PackageManager.PERMISSION_GRANTED) {
                    File folder = new File(externa, rutes);
                    if(!folder.exists()){
                        folder.mkdirs();
                    }
                } else {
                    Toast.makeText(this, "É NECESARIO O PERMISO PARA CHAMAR POR TELÉFONO", Toast.LENGTH_LONG).show();
                }
                return;
            }
        }
    }
}