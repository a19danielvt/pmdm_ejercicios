package com.example.i4b_a19danielvt;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;

import org.w3c.dom.ProcessingInstruction;
import org.w3c.dom.Text;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

public class VentanaFragment extends DialogFragment {

    Bundle datos;

    CharSequence[] opciones;
    boolean[] selecciones;
    boolean[] temp;
    String marcadas;


    public VentanaFragment(Bundle datos){
        this.datos = datos;

        inicializarVariables();
    }

    public void inicializarVariables(){
        opciones = datos.getCharSequenceArray("OPCIONES");
        selecciones = datos.getBooleanArray("SELECCIONES");
        marcadas = datos.getString("MARCADAS");
    }

    public Dialog onCreateDialog(Bundle savedInstanceState) {
        temp = new boolean[selecciones.length];

        for(int i = 0; i < temp.length; i++)
            temp[i] = selecciones[i];

        AlertDialog.Builder ventana = new AlertDialog.Builder(getContext());
        ventana.setIcon(android.R.drawable.ic_dialog_info)
                .setTitle("Opciones")
                .setMultiChoiceItems(opciones, temp, new DialogInterface.OnMultiChoiceClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                        Toast.makeText(getContext(), "Seleccionaste: " + opciones[which],
                                Toast.LENGTH_SHORT).show();
                    }
                })
                .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        selecciones = temp;
                        marcadas = "Opciones seleccionadas:\n";
                        for(int i = 0; i < selecciones.length; i++){
                            if(selecciones[i])
                                marcadas += opciones[i] + " ";
                        }
                    }
                });

        return ventana.create();
    }

    @Override
    public void onDestroy() {
        ((Principal)this.getActivity()).actualizarLabel(marcadas);
        ((Principal)this.getActivity()).igualarSelecciones(selecciones);
        super.onDestroy();
    }

}


