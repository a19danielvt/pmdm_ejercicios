package com.example.i4b_a19danielvt;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.FragmentActivity;

import android.content.DialogInterface;
import android.os.*;
import android.util.Log;
import android.view.View;
import android.widget.*;

import java.io.*;
import java.util.ArrayList;

public class Principal extends FragmentActivity{

    Bundle datos;

    String[] opcionesDefecto = {"manzana", "hamburguesa", "pasta", "lechuga"};
    boolean[] seleccionesDefecto = {false, true, true, false};
    ArrayList<String> op;
    ArrayList<String> se;
    CharSequence[] opciones;
    boolean[] selecciones;
    boolean[] temp;

    boolean sdDisponhible = false;
    boolean sdAccesoEscritura = false;
    String nombreArchivoOpciones = "opciones.txt";
    String nombreArchivoSelecciones = "selecciones.txt";
    String nombreArchivoLabel = "label.txt";
    File archivoOpciones;
    File archivoSelecciones;
    File archivoLabel;


    EditText texto;
    Button añadir;
    TextView label;

    VentanaFragment f;

    int COD_FRAGMENT = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_principal);

        añadir = (Button) findViewById(R.id.añadir);
        texto = (EditText) findViewById(R.id.texto);
        label = (TextView) findViewById(R.id.label);

        comprobarEstadoSD();
        establecerDirectorioFicheiro();

        if (!(archivoOpciones.exists() && archivoSelecciones.exists()))
            escribirPredeterminado();

        if(archivoLabel.exists()){
            Toast.makeText(this, "Existe", Toast.LENGTH_SHORT).show();
            try{
                BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(archivoLabel)));

                //label.setText("Opciones seleccionadas:\n");
                String linea;
                while((linea = br.readLine()) != null)
                    label.append(linea);

                br.close();
            }catch(Exception e){
                System.out.println(e.getMessage());
            }
        }

        añadirValores();

        datos = new Bundle();

    }

    public void comprobarEstadoSD() {
        String estado = Environment.getExternalStorageState();

        if (estado.equals(Environment.MEDIA_MOUNTED)) {
            sdDisponhible = true;
            sdAccesoEscritura = true;
        } else if (estado.equals(Environment.MEDIA_MOUNTED_READ_ONLY))
            sdDisponhible = true;
    }

    public void establecerDirectorioFicheiro() {
        if (sdDisponhible) {
            archivoOpciones = new File(getExternalFilesDir(null).getAbsolutePath(), nombreArchivoOpciones);
            archivoSelecciones = new File(getExternalFilesDir(null).getAbsolutePath(), nombreArchivoSelecciones);
            archivoLabel = new File(getExternalFilesDir(null).getAbsolutePath(), nombreArchivoLabel);
        }
    }

    public void escribirPredeterminado() {
        if (sdAccesoEscritura) {
            try {
                // opciones
                OutputStreamWriter osw = new OutputStreamWriter(new FileOutputStream(archivoOpciones, true));

                for (String s : opcionesDefecto)
                    osw.write(s + "\n");

                osw.close();

                // selecciones
                OutputStreamWriter osw1 = new OutputStreamWriter(new FileOutputStream(archivoSelecciones, true));

                for (boolean b : seleccionesDefecto)
                    osw1.write(b + "\n");

                osw1.close();
            } catch (Exception ex) {
                Log.e("SD", "Error escribindo no ficheiro");
            }
        } else
            Toast.makeText(this, "A tarxeta SD non está en modo acceso escritura", Toast.LENGTH_SHORT).show();
    }

    /**
     * método que lee los archivos y crea dos arrays, CharSequence[] y boolean[]
     * que son los usados en las ventanas de diálogo
     */
    public void añadirValores() {
        op = leerOpciones();
        se = leerSelecciones();

        crearArraysFinales();
    }

    /**
     * lee el archivo de opciones y guarda los datos en un arrayList
     *
     * @return arrayList de las opciones
     */
    public ArrayList<String> leerOpciones() {
        ArrayList<String> op = new ArrayList<>();

        try {
            //opciones
            BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(archivoOpciones)));

            String linea;
            while ((linea = br.readLine()) != null)
                op.add(linea);

            br.close();
        } catch (Exception e) {
            System.out.println("copiando datos de los archivos");
        }

        return op;
    }

    /**
     * lee el archivo de selecciones y guarda los datos en un arrayList
     *
     * @return arrayList de las selecciones
     */
    public ArrayList<String> leerSelecciones() {
        ArrayList<String> se = new ArrayList<>();

        try {
            //opciones
            BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(archivoSelecciones)));

            String linea;
            while ((linea = br.readLine()) != null)
                se.add(linea);

            br.close();
        } catch (Exception e) {
            System.out.println("copiando datos de los archivos");
        }

        return se;
    }

    /**
     * crea los arrays utilizados en las ventanas de diálogo a partir de los arrayList
     * previamente creados en la lectura de los archivos
     */
    public void crearArraysFinales() {
        // array final de opciones
        opciones = new CharSequence[op.size()];

        for (int i = 0; i < op.size(); i++)
            opciones[i] = op.get(i);

        // array final de selecciones
        selecciones = new boolean[se.size()];

        for (int i = 0; i < se.size(); i++) {
            if (se.get(i).equals("true")) selecciones[i] = true;
            else selecciones[i] = false;
        }
    }

    public void clickDialog(View v) {
        temp = new boolean[selecciones.length];

        for(int i = 0; i < temp.length; i++)
            temp[i] = selecciones[i];

        AlertDialog.Builder ventana = new AlertDialog.Builder(this);
        ventana.setIcon(android.R.drawable.ic_dialog_info)
                .setTitle("Opciones")
                .setMultiChoiceItems(opciones, temp, new DialogInterface.OnMultiChoiceClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                        Toast.makeText(getApplicationContext(), "Seleccionaste: " + opciones[which],
                                Toast.LENGTH_SHORT).show();
                    }
                })
                .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        selecciones = temp;
                        String marcadas = "Opciones seleccionadas:\n";
                        try {
                            for (int i = 0; i < selecciones.length; i++) {
                                if (i == 0) {
                                    OutputStreamWriter os = new OutputStreamWriter(new FileOutputStream(
                                            archivoSelecciones, false));

                                    os.write(selecciones[i] + "\n");
                                    os.close();
                                } else {
                                    OutputStreamWriter os = new OutputStreamWriter(new FileOutputStream(
                                            archivoSelecciones, true));

                                    os.write(selecciones[i] + "\n");
                                    os.close();
                                }

                                if(selecciones[i])
                                    marcadas += opciones[i] + " ";
                            }

                            datos.putString("MARCADAS", marcadas);
                            actualizarLabel(datos.getString("MARCADAS"));
                        } catch (Exception e) {
                            Log.e("ERROR", "Error al actualizar las selecciones");
                        }
                    }
                });

        ventana.create().show();
    }

    public void clickFragment(View v) {
        datos.putCharSequenceArray("OPCIONES", opciones);
        datos.putBooleanArray("SELECCIONES", selecciones);

        f = new VentanaFragment(datos);
        f.show(getSupportFragmentManager(), "tag");
    }

    public void clickAñadir(View v) {
        try {
            if (texto.getText().toString().equals("") || texto.getText().toString() == null)
                Toast.makeText(this, "Escribe algo para añadirlo a la lista", Toast.LENGTH_LONG).show();
            else {
                OutputStreamWriter os = new OutputStreamWriter(new FileOutputStream(archivoOpciones, true));
                os.write(texto.getText().toString() + "\n");
                os.close();
                texto.setText("");

                OutputStreamWriter os1 = new OutputStreamWriter(new FileOutputStream(archivoSelecciones, true));
                os1.write("false\n");
                os1.close();

                añadirValores();
            }
        } catch (Exception e) {
            Log.e("FALLO", "fallo al añadir datos a la lista: " + e.getMessage());
        }
    }

    public void actualizarLabel(String datos){
        label.setText(datos);
    }

    public void igualarSelecciones(boolean[] b){
        selecciones = b;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putString("MARCADAS", label.getText().toString());
        outState.putBooleanArray("SELECCIONES", selecciones);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        label.setText(savedInstanceState.getString("MARCADAS"));
        selecciones = savedInstanceState.getBooleanArray("SELECCIONES");
    }

    @Override
    protected void onStop() {
        super.onStop();
        try {
            OutputStreamWriter os1 = new OutputStreamWriter(new FileOutputStream(archivoLabel, false));
            os1.write(label.getText().toString());
            os1.close();
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
    }
}