package com.example.ud_a2a_a19danielvt;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Method;

public class Main extends Activity {

    private Spinner spinAudios;

    private boolean sdDisponible = false;
    private boolean sdAccesoEscritura = false;

    private MediaPlayer mediaplayer;
    private MediaRecorder mediaRecorder;

    private final int REQUEST_CODE_CAMARA = 13;

    private boolean perm = false;
    private int cont = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        permisos();

        spinAudios = findViewById(R.id.spinAudios);

        comprobarEstadoSD();
        mediaplayer = new MediaPlayer();

        actualizarSpin();

        Button reproducir =(Button) findViewById(R.id.btnRep);
        reproducir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(perm) {
                    String audio = (String) spinAudios.getSelectedItem();
                    if (audio != null && !audio.isEmpty()) {

                        String path = getRuteDB(false, 0).getAbsolutePath() + File.separator + audio;

                        Uri uri = Uri.parse(path);
                        cambiarAudio(uri);
                    } else {
                        actualizarSpin();
                    }
                }else{
                    permisos();
                }
            }
        });

        Button gravar =(Button) findViewById(R.id.btnGra);
        gravar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(perm){
                    mediaRecorder = new MediaRecorder();
                    String rutaDestino = getRuteDB(false,0).getAbsolutePath() +File.separator+ "record.3gp";
                    mediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
                    mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
                    mediaRecorder.setMaxDuration(10000);
                    mediaRecorder.setAudioEncodingBitRate(32768);
                    mediaRecorder.setAudioSamplingRate(8000);
                    mediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);
                    mediaRecorder.setOutputFile(rutaDestino);
                    try {
                        mediaRecorder.prepare();
                    } catch (Exception e) {
                        mediaRecorder.reset();
                    }
                    mediaRecorder.start();
                    abrirDialogo("GRABAR");
                }else{
                    permisos();
                }
            }
        });

        Button foto =(Button) findViewById(R.id.btnFot);
        foto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(perm){
                    File ruta = getRuteDB(false,1);
                    cont++;
                    File arquivo = new File(ruta,"foto"+cont+".jpg");

                    Intent intento = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

                    intento.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(arquivo));

                    startActivityForResult(intento, REQUEST_CODE_CAMARA);
                }else{
                    permisos();
                }
            }
        });
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE_CAMARA) {
            if (resultCode == RESULT_OK) {
                    File ruta = getRuteDB(false,1);
                    File arquivo = new File(ruta,"foto" + cont + ".jpg");
                    if (!arquivo.exists()) return;

                    ImageView imgview = (ImageView) findViewById(R.id.imgView);
                    Bitmap bitmap = BitmapFactory.decodeFile(arquivo.getAbsolutePath());
                    imgview.setImageBitmap(bitmap);
            } else if (resultCode == RESULT_CANCELED) {
                Toast.makeText(this, "Foto cancelada", Toast.LENGTH_LONG).show(); // Foto cancelada
            } else {
                Toast.makeText(this, "Fallo na captura", Toast.LENGTH_LONG).show();   // Fallo na captura da foto.
            }
        }
    }
    private void abrirDialogo(String tipo) {
        if (tipo == "GRABAR") {
            AlertDialog.Builder dialog = new AlertDialog.Builder(this)
                    .setMessage("GRABANDO").setPositiveButton(
                            "PULSA PARA PARAR",
                            new DialogInterface.OnClickListener() {

                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    mediaRecorder.stop();
                                    mediaRecorder.release();
                                    mediaRecorder = null;
                                    actualizarSpin();
                                }
                            });
            dialog.show();
        }
    }

    private void comprobarEstadoSD() {
        String estado = Environment.getExternalStorageState();
        Log.e("SD", estado);

        if (estado.equals(Environment.MEDIA_MOUNTED)) {
            sdDisponible = true;
            sdAccesoEscritura = true;
        } else if (estado.equals(Environment.MEDIA_MOUNTED_READ_ONLY))
            sdDisponible = true;
    }

    private boolean isAlreadyInstalled(){
        File dir = new File(Environment.getExternalStorageDirectory().getAbsolutePath(),"UD2-A2A");
        if (!dir.exists()) {
            getRuteDB(true,0);
            return false;
        }

        return true;
    }
    private File getRuteDB(boolean create,int i){
        if(sdDisponible){
            File main =new File(Environment.getExternalStorageDirectory().getAbsolutePath(),"UD2-A2A");
            Log.i("VALOR pai", main.getAbsolutePath());

            if (!main.exists() && create) {
                boolean b = main.mkdir();
                Log.i("Creapai", String.valueOf(b));
            }

            File carpeta1 = new File(main, "MUSICA");
            if (!carpeta1.exists() && create) {
                carpeta1.mkdir();
            }

            File carpeta2 = new File(main, "FOTO");
            if (!carpeta2.exists() && create) {
                carpeta2.mkdir();
            }

            if(i==1)
                return carpeta2;

            return carpeta1;
        }
        return null;
    }

    public void actualizarSpin(){
        if (sdDisponible) {
            File dirFicheiroSD=getRuteDB(false,0);

            if (dirFicheiroSD!=null && dirFicheiroSD.exists()) {
                String[] audios = dirFicheiroSD.list();
                setSpinner(audios);
            }else
                Toast.makeText(this, "El directorio no existe", Toast.LENGTH_SHORT).show();
        } else
            Toast.makeText(this, "A tarxeta SD non está dispoñible", Toast.LENGTH_SHORT).show();
    }

    private void setSpinner(String[] audios){//OK
        ArrayAdapter<String> adaptador = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, audios);
        adaptador.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinAudios.setAdapter(adaptador);
        spinAudios.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {

            }
            @Override
            public void onNothingSelected(AdapterView<?> arg0) {

            }
        });
    }

    private void cambiarAudio(Uri uri){
        Log.i("cambiarAudio()","Entrando");
        AlertDialog.Builder dialog = new AlertDialog.Builder(this)
                .setMessage("REPRODUCIENDO").setCancelable(false).setPositiveButton(
                        "PULSA PARA PARAR",
                        new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if (mediaplayer.isPlaying()) mediaplayer.stop();
                                if (mediaplayer != null) mediaplayer.release();
                                mediaplayer = null;
                            }
                        });
        AlertDialog reproduciendo= dialog.create();

        try {
            mediaplayer = new MediaPlayer();
            mediaplayer.setDataSource(getApplicationContext(),uri);
            mediaplayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            mediaplayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    reproduciendo.dismiss();
                }
            });
            mediaplayer.prepare();
            mediaplayer.start();
        } catch (Exception e) {
            Log.e("MULTIMEDIA","Error cambiando audio");
        }
        reproduciendo.show();
    }

    private void copiarDeAssets(){
        if (isAlreadyInstalled()) {
            Log.i("CopiarAssets?","AlreadyInstalled");
            return;
        }

        File dirAudios = new File(Environment.getExternalStorageDirectory().getAbsolutePath(),"UD2-A2A/MUSICA");
        InputStream ips;
        try {
            String[] audios = getAssets().list("");
            for (int i = 0; i < audios.length; i++) {
                if (!audios[i].contains("audio")) {
                    break;
                }
                ips = getAssets().open(audios[i]);
                OutputStream ops = new FileOutputStream(dirAudios + "/" + audios[i], false);
                int read;

                while ((read = ips.read()) != -1) {
                    ops.write(read);
                }

                ops.flush();
                ops.close();
                ips.close();
            }

            actualizarSpin();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        boolean todoOK = false;
        if (requestCode == 1) {
            todoOK = true;
            for (int i = 0; i < permissions.length; i++) {
                int grantResult = grantResults[i];
                if (grantResult == PackageManager.PERMISSION_GRANTED) {

                } else {
                    todoOK=false;
                }
            }

            if(todoOK){
                perm = true;
                copiarDeAssets();
            }else{
                perm = false;
                Toast.makeText(getApplicationContext(),"Acepta todos los permisos si queres continuar",Toast.LENGTH_LONG).show();
            }
        }
    }

    public void permisos(){
        if(Build.VERSION.SDK_INT == 28){
            int wrte=checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE);
            int read=checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE);
            int cam=checkSelfPermission(Manifest.permission.CAMERA);
            int rec=checkSelfPermission(Manifest.permission.RECORD_AUDIO);
            if(wrte == PackageManager.PERMISSION_GRANTED &&  read == PackageManager.PERMISSION_GRANTED
                && cam == PackageManager.PERMISSION_GRANTED && rec == PackageManager.PERMISSION_GRANTED){
                perm = true;
            }else{
                requestPermissions(new String[]{
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.CAMERA,
                        Manifest.permission.RECORD_AUDIO
                },1);
            }

        }else{
            perm = true;
            copiarDeAssets();
        }
        if (Build.VERSION.SDK_INT >= 24) {
            try {
                Method m = StrictMode.class.getMethod("disableDeathOnFileUriExposure");
                m.invoke(null);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mediaplayer.isPlaying()) mediaplayer.stop();
        if (mediaplayer != null) mediaplayer.release();
        mediaplayer = null;
    }
}