package com.example.a2dam_2021_2term_final_vieites_torres_daniel;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.util.Xml;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import org.xmlpull.v1.XmlPullParser;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

public class CasesActivity extends AppCompatActivity {

    EditText url;
    Button download;

    String urlDescargas = "https://informatica.iessanclemente.net/pmdm/";
    String textoDefecto = "cases.xml";
    String name;
    String id;

    ArrayList<Cases> casos = new ArrayList<>();
    String casesFile = "cases.txt";
    String downloads = Environment.getExternalStoragePublicDirectory(
            Environment.DIRECTORY_DOWNLOADS).getAbsolutePath();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cases);

        url = findViewById(R.id.url);
        url.setText(textoDefecto);

        Intent intent = getIntent();
        name = intent.getExtras().getString("NAME");
        id = intent.getExtras().getString("ID");

        TextView hos = findViewById(R.id.hospital);
        hos.setText(id + " -> " + name);

        download = findViewById(R.id.download);
        download.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Descargas des = new Descargas();
                des.execute(id);
            }
        });
    }

    public void descargarXml(){
        if(!isConnected()){
            Toast.makeText(this, "No hay conexión a internet", Toast.LENGTH_SHORT).show();
            return;
        }

        URL url_conn = null;
        try {
            url_conn = new URL(urlDescargas + url.getText().toString());
        } catch (Exception e) {
            Log.e("ERROR", "Error con la URL");
            return;
        }
        String archivo =  downloads + "/" + casesFile;

        try{
            HttpURLConnection conn = (HttpURLConnection) url_conn.openConnection();
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(15000);
            conn.setRequestMethod("POST");
            conn.setDoInput(true);

            conn.connect();

            int response = conn.getResponseCode();
            if (response != HttpURLConnection.HTTP_OK){
                Toast.makeText(this, "No se puede acceder a la url", Toast.LENGTH_LONG).show();
                return;
            }

            OutputStream os = new FileOutputStream(archivo);
            InputStream in = conn.getInputStream();

            byte[] data = new byte[1024];
            int count;
            while((count = in.read(data)) != -1){
                os.write(data, 0, count);
            }

            os.flush();
            os.close();
            in.close();
            conn.disconnect();
            Log.i("DESCARGADO", ":)");
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public boolean isConnected(){
        ConnectivityManager conn = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo net = conn.getActiveNetworkInfo();

        if(net != null && net.isConnected()){
            return true;
        }

        return false;
    }

    public void procesarXml(File archivo){
        try{
            Cases caso = null;

            InputStream is = new FileInputStream(archivo);

            XmlPullParser parser = Xml.newPullParser();
            parser.setInput(is, "UTF-8");

            int tag = parser.nextTag();
            while(tag != XmlPullParser.END_DOCUMENT){
                if(tag == XmlPullParser.START_TAG){
                    if(parser.getName().equals("case")){
                        caso = new Cases();

                        parser.nextTag();
                        caso.setHid(parser.nextText());

                        parser.nextTag();
                        caso.setDate(parser.nextText());

                        tag = parser.nextTag();
                        while(parser.getName().equals("ncases")){
                            if(parser.getAttributeValue(null, "type").equals("room")) {
                                caso.setRoomCases(Integer.parseInt(parser.nextText()));
                            }else{
                                caso.setICUCases(Integer.parseInt(parser.nextText()));
                            }
                            tag = parser.nextTag();
                        }
                    }
                }
                if(tag == XmlPullParser.END_TAG){
                    if(parser.getName().equals("case")){
                        casos.add(caso);
                    }
                }

                tag = parser.next();
            }

            is.close();
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    private class Descargas extends AsyncTask<String, Void, Boolean> {

        @Override
        protected Boolean doInBackground(String... strings) {
            descargarXml();

            File cases = new File(downloads + "/" + casesFile);

            procesarXml(cases);

            TF_A19DanielVT.management.insertCases(casos, strings[0]);

            return true;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
        }
    }
}