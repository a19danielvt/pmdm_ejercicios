package com.example.a2dam_2021_2term_final_vieites_torres_daniel;

import android.content.Intent;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.Spinner;
import android.widget.VideoView;

import androidx.appcompat.app.AppCompatActivity;

import java.io.File;
import java.lang.reflect.Method;
import java.util.ArrayList;

public class Multimedia extends AppCompatActivity {

    Spinner spinner;
    Button grabar, takePhoto;
    VideoView video;
    ImageView photo;

    String folderName = "/MM/a19danielvt/ficheiros/";
    File externa = Environment.getExternalStorageDirectory();
    File folder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_multimedia);

        folder = new File(externa, folderName);
        if(!folder.exists()) folder.mkdirs();

        video = findViewById(R.id.video);
        spinner = findViewById(R.id.videos);
        grabar = findViewById(R.id.grabar);
        takePhoto = findViewById(R.id.take_photo);
        photo = findViewById(R.id.photo);

        MediaController controller = new MediaController(this);
        video.setMediaController(controller);

        grabar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                action();

                int number = 0;
                for(int i = 0; i < spinner.getCount(); i++){
                    if(spinner.getItemAtPosition(i).toString().matches("video.*")){
                        number++;
                    }
                }

                File file = new File(folder, "video" + (number + 1));
                Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(file));
                startActivityForResult(intent, 1);
            }
        });

        takePhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                action();

                int number = 0;
                for(int i = 0; i < spinner.getCount(); i++){
                    if(spinner.getItemAtPosition(i).toString().matches("photo.*")){
                        number++;
                    }
                }

                File file = new File(folder, "photo" + (number + 1));
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(file));
                startActivityForResult(intent, 2);
            }
        });

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String element = parent.getItemAtPosition(position).toString();
                if(element.matches("video.*"))
                    setVideo(element);
                else
                    setPhoto(element);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        getFiles();
    }

    public void action(){
        if (Build.VERSION.SDK_INT > 23){
            try{
                Method m = StrictMode.class.getMethod("disableDeathOnFileUriExposure");
                m.invoke(null);
            }catch(Exception e){
                e.printStackTrace();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(resultCode == RESULT_CANCELED) return;

        getFiles();
    }

    public void setVideo(String videoName){
        File archivoVideo = new File(folder, videoName);
        video.setVisibility(View.VISIBLE);
        photo.setVisibility(View.GONE);
        video.setVideoURI(Uri.fromFile(archivoVideo));
    }

    public void setPhoto(String photoName){
        File archivoPhoto = new File(folder, photoName);
        photo.setVisibility(View.VISIBLE);
        video.setVisibility(View.GONE);
        photo.setImageBitmap(BitmapFactory.decodeFile(archivoPhoto.getAbsolutePath()));
    }

    public void getFiles(){
        File[] files = folder.listFiles();
        if(files.length == 0) return;

        ArrayList<String> allFiles = new ArrayList<>();

        for(File file : files){
            allFiles.add(Uri.parse(file.getAbsolutePath()).getLastPathSegment());
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, allFiles);
        spinner.setAdapter(adapter);
    }
}