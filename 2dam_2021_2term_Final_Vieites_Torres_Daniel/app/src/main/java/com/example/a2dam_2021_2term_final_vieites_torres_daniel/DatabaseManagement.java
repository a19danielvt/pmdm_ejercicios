package com.example.a2dam_2021_2term_final_vieites_torres_daniel;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

public class DatabaseManagement extends SQLiteOpenHelper {

    SQLiteDatabase db;

    final static String DATABASE_NAME = "hc_a19danielvt";

    // TABLES
    final static String TABLE_HOSPITAL = "hospital";
    final static String TABLE_CASES = "cases";

    // COLUMNS
    final static String HID = "hid";
    final static String HNAME = "hname";
    final static String TEL = "tel";
    final static String DATE = "date";

    public DatabaseManagement(Context context) {
        super(context, DATABASE_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public void insertHospitals(ArrayList<Hospital> hospitales){
        for(Hospital h : hospitales){
            if(getHospital(h.getHid()) == null){
                db.execSQL("insert into " + TABLE_HOSPITAL + " (" + HID + ", " + HNAME + ") values (\"" +
                        h.getHid() + "\", \"" + h.getHname() + "\")");
            }
        }
    }

    public Hospital getHospital(String hid){
        Cursor cursor = db.rawQuery("select * from " + TABLE_HOSPITAL + " where " + HID + "=?",
                new String[]{hid});

        Hospital hos = null;
        if(cursor.moveToFirst()){
            hos = new Hospital();
            while(!cursor.isAfterLast()){
                hos.setHid(cursor.getString(0));
                hos.setHname(cursor.getString(1));
                hos.setTel(cursor.getString(2));

                cursor.moveToNext();
            }
        }

        return hos;
    }

    public String getTel(String nombre){
        Cursor cursor = db.rawQuery("select " + TEL + " from " + TABLE_HOSPITAL + " where " + HNAME + "=?",
                new String[]{nombre});

        String tel = null;

        if(cursor.moveToFirst()){
            tel = cursor.getString(0);
        }

        return tel;
    }

    public String getHid(String nombre){
        Cursor cursor = db.rawQuery("select " + HID + " from " + TABLE_HOSPITAL + " where " + HNAME + "=?",
                new String[]{nombre});

        cursor.moveToFirst();

        return cursor.getString(0);
    }

    public void saveTel(String tel, String nombre){
        db.execSQL("update " + TABLE_HOSPITAL + " set " + TEL + "=? where " + HNAME + "=?",
                new String[]{tel, nombre});
    }

    public void insertCases(ArrayList<Cases> casos, String hid){
        for(Cases c : casos){
            if(c.getHid().equals(hid) && !existsCase(c.getHid(), c.getDate())){
                db.execSQL("insert into " + TABLE_CASES + " values (" +
                        c.getRoomCases() + ", " + c.getICUCases() + ", \"" + c.getDate() + "\", \"" +
                        c.getHid() + "\")");
            }
        }
    }

    public boolean existsCase(String hid, String date){
        Cursor cursor = db.rawQuery("select * from " + TABLE_CASES + " where " + HID + "=? and " +
                        DATE + "=?",
                new String[]{hid, date});

        Cases caso = null;
        if(cursor.moveToFirst()){
            caso = new Cases();
        }

        return caso != null;
    }

    public ArrayList<Cases> getCases(String hid){
        ArrayList<Cases> casos = new ArrayList<>();

        Cursor cursor = db.rawQuery("select * from " + TABLE_CASES + " where " + HID + "=?",
                new String[]{hid});

        Cases caso = null;
        if(cursor.moveToFirst()){
            while(!cursor.isAfterLast()){
                caso = new Cases();
                caso.setRoomCases(Integer.parseInt(cursor.getString(0)));
                caso.setICUCases(Integer.parseInt(cursor.getString(1)));
                caso.setDate(cursor.getString(2));
                caso.setHid(cursor.getString(3));

                casos.add(caso);
                cursor.moveToNext();
            }
        }

        return casos;
    }
}