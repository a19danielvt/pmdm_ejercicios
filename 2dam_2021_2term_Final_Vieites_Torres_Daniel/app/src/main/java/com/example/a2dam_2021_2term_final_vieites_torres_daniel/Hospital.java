package com.example.a2dam_2021_2term_final_vieites_torres_daniel;

public class Hospital {
    private String hid;
    private String hname;
    private String tel;

    public Hospital(){}

    public Hospital(String hid, String hname, String tel) {
        this.hid = hid;
        this.hname = hname;
        this.tel = tel;
    }

    public String getHid() {
        return hid;
    }

    public void setHid(String hid) {
        this.hid = hid;
    }

    public String getHname() {
        return hname;
    }

    public void setHname(String hname) {
        this.hname = hname;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }
}
