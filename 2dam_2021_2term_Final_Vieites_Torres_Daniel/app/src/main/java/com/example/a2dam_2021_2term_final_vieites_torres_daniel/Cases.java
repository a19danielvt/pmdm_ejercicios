package com.example.a2dam_2021_2term_final_vieites_torres_daniel;

public class Cases {
    private String hid;
    private String date;
    private int roomCases;
    private int ICUCases;

    public Cases(){}

    public Cases(String hid, String date, int roomCases, int ICUCases) {
        this.date = date;
        this.roomCases = roomCases;
        this.ICUCases = ICUCases;
    }

    public String getHid() {
        return hid;
    }

    public void setHid(String hid) {
        this.hid = hid;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getRoomCases() {
        return roomCases;
    }

    public void setRoomCases(int roomCases) {
        this.roomCases = roomCases;
    }

    public int getICUCases() {
        return ICUCases;
    }

    public void setICUCases(int ICUCases) {
        this.ICUCases = ICUCases;
    }
}
