package com.example.a2dam_2021_2term_final_vieites_torres_daniel;

import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.util.Log;
import android.util.Xml;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import org.xmlpull.v1.XmlPullParser;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

public class TF_A19DanielVT extends AppCompatActivity {

    Button save, call, cases, multimedia;
    EditText phone;
    Spinner spinner;
    ListView list;

    static DatabaseManagement management;

    ArrayList<Hospital> hospitales = new ArrayList<>();
    String urlHospitales = "https://informatica.iessanclemente.net/pmdm/hospitals.xml";

    String hosFile = "hospitales.txt";
    String descargas = Environment.getExternalStoragePublicDirectory(
            Environment.DIRECTORY_DOWNLOADS).getAbsolutePath();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        spinner = findViewById(R.id.spinner);
        multimedia = findViewById(R.id.multimedia);
        call = findViewById(R.id.call);
        phone = findViewById(R.id.phone);
        save = findViewById(R.id.save);
        cases = findViewById(R.id.cases);
        list = findViewById(R.id.list);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                showTel();
                showCases();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String tel = phone.getText().toString();

                if(!tel.matches("[ ]*")){
                    String name = spinner.getSelectedItem().toString();
                    management.saveTel(tel, name);
                    Toast.makeText(TF_A19DanielVT.this, "Teléfono guardado", Toast.LENGTH_SHORT).show();
                }else
                    Toast.makeText(TF_A19DanielVT.this, "El campo está vacío", Toast.LENGTH_SHORT).show();
            }
        });

        call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String tel = phone.getText().toString();

                if(!tel.matches("[ ]*")) {
                    Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + tel));
                    startActivity(intent);
                }else
                    Toast.makeText(TF_A19DanielVT.this, R.string.introducetel, Toast.LENGTH_SHORT).show();
            }
        });

        cases.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = spinner.getSelectedItem().toString();
                String id = management.getHid(name);

                Intent intent = new Intent(getApplicationContext(), CasesActivity.class);
                intent.putExtra("ID", id);
                intent.putExtra("NAME", name);
                startActivity(intent);
            }
        });

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String hid = management.getHid(spinner.getSelectedItem().toString());
                String fileName = getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS) + "/" + hid + "_" + position + ".txt";
                File file = new File(fileName);

                String content = parent.getItemAtPosition(position).toString();
                String toast = "Posición: " + position + "\nContenido:\n" + content;
                Toast.makeText(TF_A19DanielVT.this, toast, Toast.LENGTH_SHORT).show();
                Log.i("FILE", file.getAbsolutePath());

                try{
                    OutputStreamWriter out = new OutputStreamWriter(new FileOutputStream(file));
                    out.write(content);
                    out.close();
                }catch(Exception e){
                    e.printStackTrace();
                }
            }
        });

        multimedia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), Multimedia.class);
                startActivity(intent);
            }
        });

        permission();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menustyles, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.menustyle){
            Intent intent = new Intent(getApplicationContext(), PantallaPreferencias.class);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        showCases();
    }

    @Override
    protected void onResume() {
        super.onResume();

        SharedPreferences preferencias = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        String color = preferencias.getString("background_color", "#FF0000");
        phone.setBackgroundColor(Color.parseColor(color));

        boolean bold = preferencias.getBoolean("bold", false);
        boolean italic = preferencias.getBoolean("italic", false);

        int valor = 0;

        if(bold && italic){
            valor = 3;
        }
        else if(bold){
            valor = 1;
        }
        else if(italic){
            valor = 2;
        }

        phone.setTypeface(null, valor);
    }

    public void permission(){
        if(Build.VERSION.SDK_INT == 28){
            int external = checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE);
            int internet = checkSelfPermission(Manifest.permission.INTERNET);
            int network = checkSelfPermission(Manifest.permission.ACCESS_NETWORK_STATE);
            int call = checkSelfPermission(Manifest.permission.CALL_PHONE);
            int read = checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE);

            if(external == PackageManager.PERMISSION_GRANTED &&
                    internet == PackageManager.PERMISSION_GRANTED &&
                    network == PackageManager.PERMISSION_GRANTED &&
                    call == PackageManager.PERMISSION_GRANTED &&
                    read == PackageManager.PERMISSION_GRANTED){

                copyDB();
                initializeDB();
                Downloads d = new Downloads();
                d.execute();

            }else{
                requestPermissions(new String[]{
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.INTERNET,
                        Manifest.permission.ACCESS_NETWORK_STATE,
                        Manifest.permission.CALL_PHONE,
                        Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if(requestCode == 1){
            boolean allowed = true;
            for(int i : grantResults){
                if(i == PackageManager.PERMISSION_DENIED){
                    allowed = false;
                }
            }

            if(allowed){
                copyDB();
                initializeDB();
                Downloads d = new Downloads();
                d.execute();
            }
            else
                Toast.makeText(this, "FALTA UN PERMISO", Toast.LENGTH_SHORT).show();
        }
    }

    public void copyDB(){
        String bddestino = "/data/data/" + getPackageName() + "/databases/"
                + DatabaseManagement.DATABASE_NAME;
        File file = new File(bddestino);
        if (file.exists()) {
            Log.i("BD", "YA EXISTE LA BASE DE DATOS");
            return;
        }

        String pathbd = "/data/data/" + getPackageName()
                + "/databases/";
        File filepathdb = new File(pathbd);
        filepathdb.mkdirs();

        try {
            InputStream inputstream = getAssets().open(DatabaseManagement.DATABASE_NAME);
            OutputStream outputstream = new FileOutputStream(bddestino);

            int tamread;
            byte[] buffer = new byte[2048];

            while ((tamread = inputstream.read(buffer)) > 0) {
                outputstream.write(buffer, 0, tamread);
            }

            inputstream.close();
            outputstream.flush();
            outputstream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void initializeDB(){
        management = new DatabaseManagement(this);
        management.db = management.getWritableDatabase();
    }

    public void descargarXml(){
        if(!isConnected()){
            Toast.makeText(this, "No hay conexión a internet", Toast.LENGTH_SHORT).show();
            return;
        }

        URL url = null;
        try {
            url = new URL(urlHospitales);
        } catch (Exception e) {
            Log.e("ERROR", "Error con la URL");
            return;
        }
        String archivo =  descargas + "/" + hosFile;

        try{
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(15000);
            conn.setRequestMethod("POST");
            conn.setDoInput(true);

            conn.connect();

            int response = conn.getResponseCode();
            if (response != HttpURLConnection.HTTP_OK){
                Toast.makeText(this, "No se puede acceder a la url", Toast.LENGTH_LONG).show();
                return;
            }

            OutputStream os = new FileOutputStream(archivo);
            InputStream in = conn.getInputStream();

            byte[] data = new byte[1024];
            int count;
            while((count = in.read(data)) != -1){
                os.write(data, 0, count);
            }

            os.flush();
            os.close();
            in.close();
            conn.disconnect();
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public boolean isConnected(){
        ConnectivityManager conn = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo net = conn.getActiveNetworkInfo();

        if(net != null && net.isConnected()){
            return true;
        }

        return false;
    }

    public void processXml(File archivo){
        try{
            Hospital hos = null;

            InputStream is = new FileInputStream(archivo);

            XmlPullParser parser = Xml.newPullParser();
            parser.setInput(is, "UTF-8");

            int tag = parser.nextTag();
            while(tag != XmlPullParser.END_DOCUMENT){
                if(tag == XmlPullParser.START_TAG){
                    if(parser.getName().equals("hosp")){
                        hos = new Hospital();

                        parser.nextTag();
                        hos.setHid(parser.nextText());

                        tag = parser.nextTag();
                        hos.setHname(parser.nextText());
                    }
                }
                if(tag == XmlPullParser.END_TAG){
                    if(parser.getName().equals("hosp")){
                        hospitales.add(hos);
                    }
                }

                tag = parser.next();
            }

            is.close();
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public void hospitalesSpinner(){
        ArrayList<String> nombres = new ArrayList<>();

        for(Hospital dep : hospitales){
            nombres.add(dep.getHname());
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, nombres);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
    }

    public void showTel(){
        String hos = spinner.getSelectedItem().toString();
        String tel = management.getTel(hos) + "";

        if(tel.equals("null"))
            phone.setText("");
        else
            phone.setText(tel);
    }

    public void showCases(){
        String hid = management.getHid(spinner.getSelectedItem().toString());
        ArrayList<Cases> casos = management.getCases(hid);
        ArrayList<String> datos = new ArrayList<>();
        if(casos.size() == 0) {
            listData(datos);
            return;
        }

        datos.add("Year  Month      Room         ICU       Total");

        String dato = "";
        String mes = casos.get(0).getDate().split("/")[1];
        String year = casos.get(0).getDate().split("/")[0];
        int roomcases = 0;
        int icucases = 0;
        for(Cases caso : casos){
            String[] valoresFecha = caso.getDate().split("/");
            if(!valoresFecha[1].equals(mes) || !valoresFecha[0].equals(year)) {
                dato = year + "     " + mes + "              " + roomcases + "            " + icucases + "          " + (roomcases + icucases);
                datos.add(dato);

                roomcases = 0;
                icucases = 0;
                mes = valoresFecha[1];
                year = valoresFecha[0];
            }
            roomcases += caso.getRoomCases();
            icucases += caso.getICUCases();
        }
        dato = year + "     " + mes + "              " + roomcases + "            " + icucases + "          " + (roomcases + icucases);
        datos.add(dato);

        listData(datos);
    }

    public void listData(ArrayList<String> datos){
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, datos);
        list.setAdapter(adapter);
    }

    private class Downloads extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected Boolean doInBackground(Void... voids) {
            descargarXml();

            File hos = new File(descargas + "/" + hosFile);

            processXml(hos);

            management.insertHospitals(hospitales);
            return true;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            if(aBoolean) {
                hospitalesSpinner();
                showTel();
                showCases();
            }
        }
    }
}