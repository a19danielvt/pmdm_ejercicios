package com.example.ud_a3a_thread_a19danielvt;

import android.os.Message;
import android.util.Log;
import android.widget.Toast;

public class MiHilo extends Thread{
    private final int seconds = 10;
    private int actualSecond;

    private boolean stop;

    ClasePuente puente;

    public MiHilo(MainActivity main){
        puente = new ClasePuente(main);
    }

    public void run(){
        stop = false;
        for (int i = seconds; i > 0; i--) {
            if(stop) break;

            actualSecond = i;
            try{
                Thread.sleep(1000);
            }catch(Exception e){
                Log.e("ERROR", "error contando");
            }
        }

        Message msg = new Message();
        if(stop) msg.arg1 = 2;
        else msg.arg1 = 1;
        puente.sendMessage(msg);
    }

    public int getActualSecond(){
        return actualSecond;
    }

    public void stopThread(){
        stop = true;
    }
}
