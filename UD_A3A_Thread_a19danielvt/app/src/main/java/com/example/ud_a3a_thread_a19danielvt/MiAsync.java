package com.example.ud_a3a_thread_a19danielvt;

import android.os.AsyncTask;
import android.util.Log;

public class MiAsync extends AsyncTask<Void, Integer, Boolean> {

    private final int seconds = 10;
    private int actual_second;

    private MainActivity main;

    public MiAsync(MainActivity main){
        this.main = main;
    }

    @Override
    protected Boolean doInBackground(Void... voids) {
        for (int i = seconds; i > 0; i--) {
            actual_second = i;
            try{
                Thread.sleep(1000);
            }catch(Exception e){
                Log.e("ERROR", "parando");
            }

            if(isCancelled()) break;
        }
        return true;
    }

    @Override
    protected void onPostExecute(Boolean aBoolean) {
        if(aBoolean){
            main.number.setText("SE ACABÓ EL TIEMPO!!");
        }
    }

    @Override
    protected void onCancelled() {
        main.check(actual_second);
        main.number.setText("LO HAS PARADO");
    }
}
