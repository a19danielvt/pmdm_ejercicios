package com.example.ud_a3a_thread_a19danielvt;

import android.os.Handler;
import android.os.Message;
import android.widget.TextView;
import android.widget.Toast;

import java.lang.ref.WeakReference;

public class ClasePuente extends Handler {
    private WeakReference<MainActivity> target = null;

    public ClasePuente(MainActivity mTarget){
        target = new WeakReference<MainActivity>(mTarget);
    }

    public void handleMessage(Message msg){
        MainActivity main = target.get();

        TextView number = (TextView) main.findViewById(R.id.number);

        if(msg.arg1 == 1){
            number.setText("SE ACABÓ EL TIEMPO!!");
        }
        if(msg.arg1 == 2){
            number.setText("LO HAS PARADO");
        }
    }
}
