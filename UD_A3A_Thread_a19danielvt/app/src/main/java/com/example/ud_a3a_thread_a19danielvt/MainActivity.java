package com.example.ud_a3a_thread_a19danielvt;

import androidx.appcompat.app.AppCompatActivity;

import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    TextView number;

    MiHilo hilo = null;
    MiAsync async = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        number = (TextView) findViewById(R.id.number);
    }

    public void startThread(View v){
        if((hilo == null || !hilo.isAlive()) &&
                (async == null || async.getStatus() == AsyncTask.Status.FINISHED)){
                hilo = new MiHilo(this);
                hilo.start();
                number.setText(Integer.toString(5 + (int)(Math.random() * 6)));
        }
        else
            Toast.makeText(this, "ESPERA A QUE TERMINE", Toast.LENGTH_SHORT).show();
    }

    public void stopThread(View v){
        if(hilo != null && hilo.isAlive()){
            hilo.stopThread();
            check(hilo.getActualSecond());
        }
    }

    public void startAsync(View v){
        if((async == null || async.getStatus() == AsyncTask.Status.FINISHED) &&
                (hilo == null || !hilo.isAlive())){
                async = new MiAsync(this);
                async.execute();
                number.setText(Integer.toString(5 + (int)(Math.random() * 6)));
        }
        else
            Toast.makeText(this, "ESPERA A QUE TERMINE", Toast.LENGTH_SHORT).show();
    }

    public void stopAsync(View v){
        if(async != null && async.getStatus() == AsyncTask.Status.RUNNING){
            async.cancel(true);
        }
    }
    
    public void check(int second){
        if (second == Integer.parseInt(number.getText().toString()))
            Toast.makeText(this, "LO HAS CONSEGUIDO!! -> " + second, Toast.LENGTH_SHORT).show();
        else
            Toast.makeText(this, "HAS FALLADO!! -> " + second, Toast.LENGTH_SHORT).show();
    }
}