package com.example.i3c_a19danielvt;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.widget.Toast;

import androidx.fragment.app.DialogFragment;

public class TresBotones extends DialogFragment {
    public Dialog onCreateDialog(Bundle saveInstanceState){
        AlertDialog.Builder ventana = new AlertDialog.Builder(getActivity());
        ventana.setIcon(android.R.drawable.ic_dialog_info)
                .setTitle("Encuesta")
                .setMessage("Todos los de clase van a hacer bien esta tarea.")
                .setCancelable(false)
                .setPositiveButton("Sí", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Toast.makeText(getContext(), "Pulsaste 'Sí'", Toast.LENGTH_LONG).show();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Toast.makeText(getContext(), "Pulsaste 'No'", Toast.LENGTH_LONG).show();
                    }
                })
                .setNeutralButton("No sé", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Toast.makeText(getContext(), "Pulsaste 'No sé'", Toast.LENGTH_LONG).show();
                    }
                });

        return ventana.create();
    }
}