package com.example.i3c_a19danielvt;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.widget.Toast;

import androidx.fragment.app.DialogFragment;

public class CheckBox extends DialogFragment {
    Bundle datos = new Bundle();

    public CheckBox(Bundle datos){
        this.datos = datos;
    }

    public Bundle getDatos(){
        return datos;
    }

    public Dialog onCreateDialog(Bundle savedInstanceState) {
        if(!datos.containsKey("SELECCIONADOS"))
            datos.putBooleanArray("SELECCIONADOS", new boolean[] {false, true, false, true, false, false, false});

        boolean[] seleccionados = datos.getBooleanArray("SELECCIONADOS");

        boolean[] temp = new boolean[seleccionados.length];
        for(int i = 0; i < seleccionados.length; i++){
            temp[i] = seleccionados[i];
        }
        datos.putBooleanArray("TEMP", temp);

        AlertDialog.Builder ventana = new AlertDialog.Builder(getContext());
        ventana.setIcon(android.R.drawable.ic_dialog_info)
            .setTitle("Selecciona modos de transporte");

        final String[] matriz = getResources().getStringArray(R.array.elementos_dialog_seleccion2);
        // No incluir mensaje dentro de este tipo de diálogo
        ventana.setMultiChoiceItems(matriz, datos.getBooleanArray("TEMP"), new DialogInterface.OnMultiChoiceClickListener() {
            public void onClick(DialogInterface dialog, int opcion, boolean isChecked) {
                // Evento que ocorre cando o usuario selecciona unha opción

                if (isChecked)
                    Toast.makeText(getContext(), "Seleccionaste " + matriz[opcion], Toast.LENGTH_SHORT).show();
                else
                    Toast.makeText(getContext(), "Deseleccionaches " + matriz[opcion], Toast.LENGTH_SHORT).show();
            }
        })
        .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int boton) {
                datos.putBooleanArray("SELECCIONADOS", datos.getBooleanArray("TEMP"));

                String texto = "Seleccionaste:";

                for(int i = 0; i < datos.getBooleanArray("TEMP").length; i++){
                    if(datos.getBooleanArray("TEMP")[i])
                        texto += "\n" + matriz[i];
                }

                Toast.makeText(getContext(), texto, Toast.LENGTH_LONG).show();
            }
        })
        .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int boton) {
                Toast.makeText(getContext(), "Pulsaste 'Cancelar'", Toast.LENGTH_LONG).show();
            }
        });

        return ventana.create();
    }
}
