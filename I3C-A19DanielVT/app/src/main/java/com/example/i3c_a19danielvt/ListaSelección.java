package com.example.i3c_a19danielvt;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.icu.text.LocaleDisplayNames;
import android.os.Bundle;
import android.widget.Toast;

import androidx.fragment.app.DialogFragment;

public class ListaSelección extends DialogFragment {

    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder ventana = new AlertDialog.Builder(getActivity());
        ventana.setIcon(android.R.drawable.ic_dialog_alert)
            .setTitle("Escolle unha opción")
            .setItems(R.array.elementos_dialog_seleccion, new DialogInterface.OnClickListener() {

                public void onClick(DialogInterface dialog, int opcion) {
                    // O usuario selecciona unha das opcións do listado
                    String[] opcions = getResources().getStringArray(R.array.elementos_dialog_seleccion);
                    Toast.makeText(getContext(), "Seleccionaches: '" + opcions[opcion] + "'", Toast.LENGTH_LONG).show();
                }
            });

        return ventana.create();
    }
}
