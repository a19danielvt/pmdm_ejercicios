package com.example.i3c_a19danielvt;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.DialogFragment;

public class EntradaTexto extends DialogFragment {
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Preparamos lo necesario para inflar el fichero XML de la ventana
        String infService = Context.LAYOUT_INFLATER_SERVICE;
        LayoutInflater li = (LayoutInflater) getContext().getSystemService(infService);

        // Inflamos el XML
        View inflador = li.inflate(R.layout.fragment_entrada_texto, null);

        // Componentes del diálogo
        final TextView etNome = (TextView) inflador.findViewById(R.id.et_nome);
        final TextView etContrasinal = (TextView) inflador.findViewById(R.id.et_contrasinal);

        AlertDialog.Builder ventana = new AlertDialog.Builder(getContext());
        ventana.setTitle("Indica usuario e contrasinal")
        // Asignamos el contenido dentro del diálogo
            .setView(inflador)
        // No se pueden incluír mensajes en este tipo de diálogo

            .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int boton) {
                    Toast.makeText(getContext(), "Escribiste nombre: '" + etNome.getText().toString() +
                                    "'. Contraseña: '" + etContrasinal.getText().toString() +
                                    "' y pulsaste 'Aceptar'",
                            Toast.LENGTH_LONG).show();
                }
            })
            .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int boton) {
                    Toast.makeText(getContext(), "Pulsaste en 'Cancelar'", Toast.LENGTH_LONG).show();
                }
            });

        return ventana.create();
    }
}