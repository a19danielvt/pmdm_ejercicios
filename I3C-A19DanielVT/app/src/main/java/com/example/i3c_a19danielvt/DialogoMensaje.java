package com.example.i3c_a19danielvt;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.KeyEvent;

import androidx.fragment.app.DialogFragment;

public class DialogoMensaje extends DialogFragment {
    public Dialog onCreateDialog(Bundle saveInstanceState){
        AlertDialog.Builder ventana = new AlertDialog.Builder(getActivity());
        ventana.setIcon(android.R.drawable.ic_dialog_email)
                .setTitle("Atención")
                .setMessage("Nuevo mensaje. Pulsa el botón 'Back' para volver a la pantalla principal");

        setCancelable(false);

        ventana.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                if(keyCode == event.KEYCODE_BACK){
                    dismiss();
                    return true;
                }
                return false;
            }
        });

        return ventana.create();
    }
}
