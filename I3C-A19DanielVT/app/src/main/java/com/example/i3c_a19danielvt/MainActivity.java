package com.example.i3c_a19danielvt;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

import android.app.Dialog;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;

public class MainActivity extends FragmentActivity {

    Bundle datosRadioButton = new Bundle();
    Bundle datosCheckBox = new Bundle();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onBotonClick(View v){
        final int mensaje = R.id.btn_dialogo;
        final int tresBotones = R.id.btn_diag_tres_botons;
        final int listaSelecc = R.id.btn_diag_list_selecc;
        final int radioButton = R.id.btn_diag_radio_button;
        final int checkbox = R.id.btn_diag_checkbox;
        final int entradaTexto = R.id.btn_diag_entrada_texto;

        switch(v.getId()) {
            case mensaje:
                DialogoMensaje dialogo = new DialogoMensaje();
                dialogo.show(getSupportFragmentManager(), "mensaje");
                break;

            case tresBotones:
                TresBotones tb = new TresBotones();
                tb.show(getSupportFragmentManager(), "tb");
                break;

            case listaSelecc:
                ListaSelección ls = new ListaSelección();
                ls.show(getSupportFragmentManager(), "ts");
                break;

            case radioButton:
                RadioButton rb = new RadioButton(datosRadioButton);
                rb.show(getSupportFragmentManager(), "rb");
                break;

            case checkbox:
                CheckBox cb = new CheckBox(datosCheckBox);
                cb.show(getSupportFragmentManager(), "cb");
                System.out.println("HOLA");
                break;

            case entradaTexto:
                EntradaTexto et = new EntradaTexto();
                et.show(getSupportFragmentManager(), "et");
                break;

            default:
                break;
        }
    }

    public void onSaveInstanceState(Bundle outState) {
        outState.putInt("CHECKED", datosRadioButton.getInt("CHECKED"));
        outState.putBooleanArray("SELECCIONADOS", datosCheckBox.getBooleanArray("SELECCIONADOS"));
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        datosRadioButton.putInt("CHECKED", savedInstanceState.getInt("CHECKED"));
        datosCheckBox.putBooleanArray("SELECCIONADOS", savedInstanceState.getBooleanArray("SELECCIONADOS"));
    }
}