package com.example.i3c_a19danielvt;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;

public class RadioButton extends DialogFragment {
    private Bundle datos;

    public RadioButton(Bundle datos){
        this.datos = datos;
    }

    public Bundle getDatos(){
        return datos;
    }

    public Dialog onCreateDialog(Bundle savedInstanceState) {
        if(!datos.containsKey("CHECKED"))
            datos.putInt("CHECKED", 0);

        AlertDialog.Builder ventana = new AlertDialog.Builder(getActivity());
        ventana.setIcon(android.R.drawable.ic_dialog_info)
            .setTitle("Selecciona un smartpohone");

        // No se incluye mensaje en este tipo de diálogo
        final CharSequence[] smartphones = { "iPhone", "Blackberry", "Android" };

        ventana.setSingleChoiceItems(smartphones, datos.getInt("CHECKED"), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int opcion) {
                // Evento que ocorre cando o usuario selecciona una opción
                datos.putInt("TEMP", opcion);
                Toast.makeText(getContext(), smartphones[opcion], Toast.LENGTH_SHORT).show();
            }
        });
        ventana.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int boton) {
                datos.putInt("CHECKED", datos.getInt("TEMP"));
                Toast.makeText(getContext(), "Seleccionaste " + smartphones[datos.getInt("CHECKED")], Toast.LENGTH_SHORT).show();
            }
        });
        ventana.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int boton) {
                Toast.makeText(getContext(), "Pulsaste 'Cancelar'", Toast.LENGTH_SHORT).show();
            }
        });

        return ventana.create();
    }
}
