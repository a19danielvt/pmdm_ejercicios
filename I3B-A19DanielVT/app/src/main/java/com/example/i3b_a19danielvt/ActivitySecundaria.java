package com.example.i3b_a19danielvt;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class ActivitySecundaria extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_secundaria);
    }

    public void onClickCerrar(View v){
        EditText texto = (EditText) findViewById(R.id.texto);
        EditText telefono = (EditText) findViewById(R.id.telefono);
        Intent intent = new Intent();

        intent.putExtra("TEXTO", texto.getText().toString());
        intent.putExtra("TELEFONO", telefono.getText().toString());

        setResult(RESULT_OK, intent);
        finish();
    }
}