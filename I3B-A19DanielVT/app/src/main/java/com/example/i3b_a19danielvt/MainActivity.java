     package com.example.i3b_a19danielvt;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentActivity;

import android.Manifest;
import android.app.SearchManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.*;

import static androidx.core.content.PermissionChecker.checkSelfPermission;

public class MainActivity extends FragmentActivity {

    private static final int COD_PETICION = 1;
    private static final int CODIGO = 111;
    Bundle datos = new Bundle();
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Listener botón principal
        Button principal = findViewById(R.id.boton);
        principal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), ActivitySecundaria.class);
                startActivityForResult(intent, COD_PETICION);
            }
        });

        // Listener onLongClick
        principal.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                final String texto = datos.getString("TEXTO");
                final String telefono = datos.getString("TELEFONO");

                AlertDialog.Builder ventana = new AlertDialog.Builder(MainActivity.this);
                ventana.setIcon(android.R.drawable.ic_dialog_info)
                        .setTitle("Escoge una opción")
                        .setItems(R.array.todasLasOpciones, new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if(which == 1){
                                    if(texto == null)
                                        buscarEnGoogle("casa");
                                    else
                                        buscarEnGoogle(texto);
                                }
                                else {
                                    if (telefono == null)
                                        noHayTelefono();
                                    else if(telefono.compareTo("") == 0)
                                        noHayTelefono();
                                    else {
                                        comprobarPermisos();
                                    }
                                }
                            }
                        });
                ventana.create().show();
                return true;
            }
        });

        // Listener botón datos
        Button botonDatos = findViewById(R.id.botonDatos);
        botonDatos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String texto = "";
                if(datos.getString("TEXTO") == null & datos.getString("TELEFONO") == null) {
                    texto += "Texto: \nTelefono: ";
                    Toast.makeText(getApplicationContext(), texto, Toast.LENGTH_SHORT).show();
                }
                else {
                    String todosLosDatos = "Texto : " + datos.getString("TEXTO") + "\nTelefono: " + datos.getString("TELEFONO");
                    Toast.makeText(getApplicationContext(), todosLosDatos, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == COD_PETICION) {
            if (resultCode == RESULT_OK) {
                datos.putString("TEXTO", data.getExtras().getString("TEXTO"));
                datos.putString("TELEFONO", data.getExtras().getString("TELEFONO"));
            }
            else
                Toast.makeText(this, "Saliste sin darle al botón cerrar", Toast.LENGTH_SHORT).show();
        }
    }

    public void buscarEnGoogle(String texto){
        texto = (texto.compareTo("") == 0)? "casa" : texto;
        Intent intent = new Intent(Intent.ACTION_WEB_SEARCH);
        intent.putExtra(SearchManager.QUERY, texto);
        startActivity(intent);
    }

    public void noHayTelefono(){
        Toast.makeText(getApplicationContext(), "No se ha introducido ningún teléfono",
                Toast.LENGTH_LONG).show();
    }

    public void comprobarPermisos(){
        if (Build.VERSION.SDK_INT >= 23) {
            System.out.println("API >= 23");
            int permiso = checkSelfPermission(Manifest.permission.CALL_PHONE);
            if (permiso == PackageManager.PERMISSION_GRANTED) {
                System.out.println("Permisos aceptados anteriormente");
                llamar(datos.getString("TELEFONO"));
            } else {
                System.out.println("Aún no se han aceptado los permisos");
                MainActivity.this.requestPermissions(new String[]{Manifest.permission.CALL_PHONE}, CODIGO);
            }
        } else {
            llamar(datos.getString("TELEFONO"));
        }
    }

    public void llamar(String telefono){
        Intent intent = new Intent(Intent.ACTION_CALL);
        intent.setData(Uri.parse("tel:" + telefono));
        startActivity(intent);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case CODIGO:
                System.out.println("Aceptando los permisos");
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    System.out.println("Permisos aceptados");
                    llamar(datos.getString("TELEFONO"));
                } else {
                }
                return;

        }
    }

    @Override
    public void onSaveInstanceState(Bundle estado) {
        if(datos.containsKey("TEXTO"))
            estado.putString("TEXTO", datos.getString("TEXTO"));

        if(datos.containsKey("TELEFONO"))
            estado.putString("TELEFONO" ,datos.getString("TELEFONO"));

        super.onSaveInstanceState(estado);
    }

    public void onRestoreInstanceState(Bundle estado){
        super.onRestoreInstanceState(estado);
        datos.putString("TEXTO", estado.getString("TEXTO"));
        datos.putString("TELEFONO", estado.getString("TELEFONO"));
    }
}