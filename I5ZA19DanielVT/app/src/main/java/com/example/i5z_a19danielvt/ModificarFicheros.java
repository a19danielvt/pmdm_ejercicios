package com.example.i5z_a19danielvt;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class ModificarFicheros extends AppCompatActivity {

    Button wa, read, list, delete;
    EditText directory, file, frases;
    RadioButton internal, sd, raw;
    CheckBox overwrite;
    File archivo, directorio;
    TextView contenido;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        wa = (Button) findViewById(R.id.writeadd);
        read = (Button) findViewById(R.id.read);
        list = (Button) findViewById(R.id.list);
        delete = (Button) findViewById(R.id.delete);
        directory = (EditText) findViewById(R.id.directory);
        file = (EditText) findViewById(R.id.file);
        frases = (EditText) findViewById(R.id.frases);
        internal = (RadioButton) findViewById(R.id.internal);
        sd = (RadioButton) findViewById(R.id.sd);
        raw = (RadioButton) findViewById(R.id.raw);
        overwrite = (CheckBox) findViewById(R.id.checkbox);
        contenido = (TextView) findViewById(R.id.contenido);
    }

    public void onButtonClick(View v){
        switch (v.getId()){
            case R.id.writeadd:{
                if(raw.isChecked()) {
                    Toast.makeText(this, R.string.writeinraw, Toast.LENGTH_SHORT).show();
                    break;
                }else if(file.getText().toString().equals("")){
                    Toast.makeText(this, R.string.typefile, Toast.LENGTH_SHORT).show();
                    break;
                }

                writeInFile();
                frases.setText("");
                break;
            }
            case R.id.read:{
                String error = "Error reading file: ";
                String dir = "";
                if(file.getText().toString().equals("")){
                    if(internal.isChecked())
                        dir = getFilesDir().getAbsolutePath() + "/" + directory.getText().toString();
                    else if(sd.isChecked())
                        dir = getExternalFilesDir(null).getAbsolutePath() + "/" + directory.getText().toString();

                    Toast.makeText(this, error + dir, Toast.LENGTH_SHORT).show();
                } else{
                    if(internal.isChecked() || sd.isChecked()) {
                        if(internal.isChecked())
                            directorioInternal();
                        else if(sd.isChecked())
                            directorioSD();

                        if (directorio.exists()) {
                            Log.e("EXISTE", "El directorio existe");
                            archivo = new File(directorio, file.getText().toString());
                            if (archivo.exists()) {
                                Log.e("ARCHIVOEXISTE", "existe");
                                readFile();
                            }else
                                Toast.makeText(this, error + archivo.getAbsolutePath(), Toast.LENGTH_SHORT).show();
                        }else{
                            Toast.makeText(this, error + directorio.getAbsolutePath(), Toast.LENGTH_SHORT).show();
                            Log.e("NO EXISTE", "El directorio no existe");
                        }
                    }else{
                        int i = getResources().getIdentifier(file.getText().toString(), "raw", getPackageName());
                        if(i == R.raw.ola || i == R.raw.adeus){
                            readFile();
                        }else{
                            Toast.makeText(this, error + file.getText().toString(), Toast.LENGTH_SHORT).show();
                        }
                    }
                }
                break;
            }
            case R.id.delete:{
                if(internal.isChecked())
                    directorioInternal();
                else if(sd.isChecked())
                    directorioSD();
                else{
                    Toast.makeText(this, "You can\'t delete in RAW memory", Toast.LENGTH_SHORT).show();
                    break;
                }

                archivo = new File(directorio, file.getText().toString());
                if(archivo.delete()){
                    Toast.makeText(this, archivo.getAbsolutePath() +
                            "has been deleted", Toast.LENGTH_SHORT).show();
                }else{
                    Toast.makeText(this, "There are problems deleting: " +
                                    archivo.getAbsolutePath() + ". Maybe is a not empty dir",
                            Toast.LENGTH_SHORT).show();
                }

                break;
            }
            case R.id.list:{
                contenido.setText("");
                String[] files = null;
                File dir = null;
                if(internal.isChecked()) {
                    directorioInternal();
                    files = directorio.list();
                }
                else if(sd.isChecked()){
                    directorioSD();
                    files = directorio.list();
                }
                else{
                    contenido.append("RAW Content:\n\n    File: adeus\n    File: ola");
                    break;
                }

                contenido.append(directorio.getAbsolutePath() + " Content:\n");

                try{
                    for(int i = 0; i < files.length; i++){
                        File subdir = new File(directorio, "/" + files[i]);
                        if(subdir.isDirectory())
                            contenido.append("\n    Subdirectory: " + files[i]);
                        else
                            contenido.append("\n    File: " + files[i]);
                    }
                }catch(Exception e){
                    Toast.makeText(this, "Error listing file " + directorio.getAbsolutePath(), Toast.LENGTH_SHORT).show();
                    Log.e("ERROR", e.getMessage());
                }
                break;
            }
        }
    }

    public void writeInFile(){
        OutputStreamWriter os = writer();
        try{
            os.write(frases.getText().toString() + "\n");
            os.close();
        }catch(Exception e){
            Log.e("ERROR", "Fallo al escribir en el fichero: " + e.getMessage());
        }
    }

    public void readFile(){
        BufferedReader br = reader();

        String linea;
        String texto = "";
        try{
            while((linea = br.readLine()) != null){
                texto += linea + "\n";
            }

            contenido.setText(texto);
        }catch (Exception e){
            Log.e("ERROR", "Fallo al escribir: " + e.getMessage());
        }
    }

    public OutputStreamWriter writer(){
        if(internal.isChecked())
            directorioInternal();
        else if(sd.isChecked())
            directorioSD();

        directorio.mkdirs();

        archivo = new File(directorio, file.getText().toString());
        System.out.println(archivo.getAbsolutePath());

        OutputStreamWriter os = null;
        try{
            os = new OutputStreamWriter(new FileOutputStream(archivo, !overwrite.isChecked()));
        }catch(Exception e){
            Log.e("ERROR", "Fallo al crear Writer: " + e.getMessage());
        }

        return os;
    }

    public BufferedReader reader(){
        BufferedReader br = null;

        if(internal.isChecked() || sd.isChecked()){
            try{
                br = new BufferedReader(new InputStreamReader(new FileInputStream(archivo)));
            }catch(Exception e){
                Log.e("ERROR", "Fallo al crear el Reader: " + e.getMessage());
            }

            return br;
        }else{
            int i = 0;
            if(file.getText().toString().equals("ola"))
                i = R.raw.ola;
            else
                i = R.raw.adeus;

            try{
                InputStream is = getResources().openRawResource(i);
                br = new BufferedReader(new InputStreamReader(is));

                return br;
            }catch(Exception e){
                Log.e("ERROR", "Fallo al crear el Reader: " + e.getMessage());
            }
        }

        return null;
    }

    public void directorioInternal(){
        if(directory.getText().equals(""))
            archivo = new File(getFilesDir(), file.getText().toString());
        else
            directorio = new File(getFilesDir(), directory.getText().toString());

    }

    public void directorioSD(){
        directorio = getExternalFilesDir(null);
        if(!directory.equals(""))
            directorio = new File(directorio, directory.getText().toString());
    }
}