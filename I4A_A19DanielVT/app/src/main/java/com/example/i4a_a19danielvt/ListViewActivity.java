package com.example.i4a_a19danielvt;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class ListViewActivity extends AppCompatActivity {

    ArrayList<String> opciones = new ArrayList<>();
    File fichero;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_view);

        Intent intent = getIntent();

        String nombreFichero = intent.getExtras().getString("FILE");
        fichero = new File(getExternalFilesDir(null).getAbsolutePath(), nombreFichero);

        ListView lista = (ListView) findViewById(R.id.listView);

        try{
            BufferedReader br = new BufferedReader(new InputStreamReader(
                    new FileInputStream(fichero)));

            String linea;

            while((linea = br.readLine()) != null)
                opciones.add(linea);

            ArrayAdapter<String> adaptador = new ArrayAdapter<>(this,
                    android.R.layout.simple_list_item_1, opciones);
            lista.setAdapter(adaptador);
            lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    String texto = "Posición: " + (position + 1) + "\nValor: " +
                            parent.getItemAtPosition(position);
                    Toast.makeText(getApplicationContext(), texto, Toast.LENGTH_SHORT).show();
                }
            });
        }catch(Exception e){System.out.println(e.getMessage());}
    }
}