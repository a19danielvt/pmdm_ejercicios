package com.example.i4a_a19danielvt;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Principal extends AppCompatActivity {

    RadioButton añadir;
    EditText texto;
    Button botonAñadir;
    Button modo;

    boolean sdDisponible = false;
    boolean sdEscritura = false;
    boolean append = true;
    File dirFichero;
    File rutaFichero;
    String nombreFichero = "fichero.txt";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.principal);

        añadir = (RadioButton) findViewById(R.id.añadir);
        texto = (EditText) findViewById(R.id.texto);
        botonAñadir = (Button) findViewById(R.id.botonAñadir);
        modo = (Button) findViewById(R.id.modo);

        estadoSD();
        establecerRuta();
    }

    public void estadoSD(){
        String estado = Environment.getExternalStorageState();

        if(estado.equals(Environment.MEDIA_MOUNTED)){
            sdDisponible = true;
            sdEscritura = true;
        }else if(estado.equals(Environment.MEDIA_MOUNTED_READ_ONLY))
            sdDisponible = true;
        else{
            Toast.makeText(this, "La SD-Card no está disponible", Toast.LENGTH_SHORT).show();
            finish();
        }
    }

    public void establecerRuta(){
        if(sdDisponible){
            dirFichero = getExternalFilesDir(null);
            rutaFichero = new File(dirFichero.getAbsolutePath(), nombreFichero);
        }else{
            Toast.makeText(this, "Esto no funciona", Toast.LENGTH_SHORT).show();
        }
    }

    public void onClickBotonAñadir(View v){
        if(sdEscritura){
            if(añadir.isChecked()) append = true;
            else append = false;

            try{
                OutputStreamWriter os = new OutputStreamWriter(new FileOutputStream(rutaFichero, append));

                SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                Date fecha = new Date();

                String temp = texto.getText() + " - " +  sdf.format(fecha);

                if(texto.getText().toString().equals("") || texto.getText() == null){
                    Toast.makeText(this, "Escribe algo para añadirlo a la lista",
                            Toast.LENGTH_LONG).show();
                }else{
                    os.write(temp +  "\n");
                    Log.i("RUTA", rutaFichero + "");
                    Log.i("TEXTO", temp);
                }
                os.close();

                texto.setText("");

            }catch(Exception e) {System.out.println(e.getMessage());}
        }
    }

    public void onClickModo(View v){
        CharSequence[] opciones = {"Spinner", "ListView"};

        AlertDialog.Builder dialogo = new AlertDialog.Builder(this);
        dialogo.setTitle("Escoge una opción")
                .setIcon(android.R.drawable.ic_dialog_info)
                .setItems(opciones, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if(which == 0)
                            otraActivity(SpinnerActivity.class);
                        else
                            otraActivity(ListViewActivity.class);
                    }
                });

        dialogo.create().show();
    }

    public void otraActivity(Class clase){
        Intent intent = new Intent(getApplicationContext(), clase);
        intent.putExtra("FILE", nombreFichero);

        startActivity(intent);
    }
}