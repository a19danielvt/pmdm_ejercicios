package com.example.i4a_a19danielvt;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class SpinnerActivity extends AppCompatActivity {

    Spinner spinner;
    ArrayList<String> opciones = new ArrayList<>();
    File fichero;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spinner);

        spinner = (Spinner) findViewById(R.id.spinner);

        Intent intent = getIntent();

        fichero = new File(getExternalFilesDir(null).getAbsolutePath(),
                intent.getExtras().getString("FILE"));

        try{
            BufferedReader bi = new BufferedReader(new InputStreamReader(new FileInputStream(fichero)));

            String linea;

            while((linea = bi.readLine()) != null)
                opciones.add(linea);

            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, opciones);
            spinner.setAdapter(adapter);

            spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    String texto = "Posición: " + (position + 1) + "\nValor: " +
                            parent.getItemAtPosition(position);
                    Toast.makeText(getApplicationContext(), texto, Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

        }catch(Exception e) {System.out.println(e.getMessage());}
    }
}