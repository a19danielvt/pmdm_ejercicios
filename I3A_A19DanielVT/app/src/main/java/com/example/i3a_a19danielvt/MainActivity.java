package com.example.i3a_a19danielvt;

import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private void chamarTelefono(){
        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setData(Uri.parse("tel:123456789"));
        startActivity(callIntent);
    }

    private void xestionarEventos(){

        Button boton = (Button)findViewById(R.id.UD08_01_btnChamar);
        boton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int permiso = checkSelfPermission(Manifest.permission.CALL_PHONE);
                if (permiso == PackageManager.PERMISSION_GRANTED){
                    chamarTelefono();
                }
                else{
                    MainActivity.this.requestPermissions( new String[]{Manifest.permission.CALL_PHONE},1);
                }
            }
        });


    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        xestionarEventos();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[],
                                           int[] grantResults) {
        switch (requestCode) {
            case 1: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    chamarTelefono();
                } else {

                    Toast.makeText(this,"É NECESARIO O PERMISO PARA CHAMAR POR TELÉFONO",Toast.LENGTH_LONG).show();
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }
}